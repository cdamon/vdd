package com.craft.vdd.message.domain;

import java.util.Map;

public interface Messages {
    String get(String key);

    String get(String key, Map<String, ? extends Object> arguments);
}
