package com.craft.vdd.error.infrastructure.primary;

import com.craft.vdd.error.domain.StandardErrorKey;
import com.craft.vdd.error.domain.VddException;
import com.craft.vdd.message.domain.Messages;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
class VddErrorHandler extends ResponseEntityExceptionHandler {

    private static final String MESSAGE_PREFIX = "error.";
    private static final String STATUS_EXCEPTION_KEY = "status-exception";

    private static final String DEFAULT_KEY = StandardErrorKey.TECHNICAL_ERROR.key();
    private static final String BAD_REQUEST_KEY = StandardErrorKey.BAD_REQUEST.key();
    private static final String NOT_AUTHENTICATED_KEY = AuthenticationErrorKey.NOT_AUTHENTICATED.key();

    private static final Logger logger = LoggerFactory.getLogger(VddErrorHandler.class);

    private final Messages messages;

    public VddErrorHandler(Messages messages) {
        this.messages = messages;
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<VddError> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception) {
        Throwable rootCause = ExceptionUtils.getRootCause(exception);
        if (rootCause instanceof VddException cause) {
            return handleVddException(cause);
        }

        VddError error = new VddError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), null);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<VddError> handleVddException(VddException exception) {
        HttpStatus status = getStatus(exception);

        trace(exception, status);

        String key = exception.key().key();
        VddError error = new VddError(key, getMessage(key, exception.arguments()), null);
        return new ResponseEntity<>(error, status);
    }

    @ExceptionHandler
    public ResponseEntity<VddError> handleResponseStatusException(ResponseStatusException exception) {
        HttpStatus status = exception.getStatus();

        trace(exception, status);

        VddError error = new VddError(STATUS_EXCEPTION_KEY, buildErrorStatusMessage(exception), null);
        return new ResponseEntity<>(error, status);
    }

    private String buildErrorStatusMessage(ResponseStatusException exception) {
        String reason = exception.getReason();

        if (StringUtils.isBlank(reason)) {
            Map<String, Object> statusArgument = Map.of("status", String.valueOf(exception.getStatus().value()));

            return getMessage(STATUS_EXCEPTION_KEY, statusArgument);
        }

        return reason;
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<VddError> handleFileSizeException(MaxUploadSizeExceededException maxUploadSizeExceededException) {
        logger.warn("File size limit exceeded: {}", maxUploadSizeExceededException.getMessage(), maxUploadSizeExceededException);

        VddError error = new VddError("server.upload-too-big", getMessage("server.upload-too-big", null), null);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<VddError> handleAccessDeniedException(AccessDeniedException accessDeniedException) {
        VddError error = new VddError("user.access-denied", getMessage("user.access-denied", null), null);
        return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
    }

    private HttpStatus getStatus(VddException exception) {
        return HttpErrorStatus.from(exception.status()).httpStatus();
    }

    private void trace(Exception exception, HttpStatus status) {
        if (status.is5xxServerError()) {
            logger.error("A server error was sent to a user: {}", exception.getMessage(), exception);

            logErrorBody(exception);
        } else {
            logger.warn("An error was sent to a user: {}", exception.getMessage(), exception);
        }
    }

    private void logErrorBody(Exception exception) {
        Throwable cause = exception.getCause();
        if (cause instanceof RestClientResponseException restCause) {
            logger.error("Cause body: {}", restCause.getResponseBodyAsString());
        }
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException exception,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request
    ) {
        logger.debug("Bean validation error {}", exception.getMessage(), exception);

        VddError error = new VddError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), getFieldsError(exception));
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<VddError> handleBeanValidationError(ConstraintViolationException exception) {
        logger.debug("Bean validation error {}", exception.getMessage(), exception);

        VddError error = new VddError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), getFieldsErrors(exception));
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(
        BindException exception,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request
    ) {
        List<VddFieldError> fieldErrors = exception.getFieldErrors().stream().map(toVddFieldError()).toList();

        VddError error = new VddError(BAD_REQUEST_KEY, getMessage(BAD_REQUEST_KEY, null), fieldErrors);

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    private ConstraintViolation<?> extractSource(FieldError error) {
        return error.unwrap(ConstraintViolation.class);
    }

    private String getMessage(String messageKey, Map<String, Object> arguments) {
        return messages.get(MESSAGE_PREFIX + messageKey, arguments);
    }

    private List<VddFieldError> getFieldsErrors(ConstraintViolationException exception) {
        return exception.getConstraintViolations().stream().map(toFieldError()).toList();
    }

    private List<VddFieldError> getFieldsError(MethodArgumentNotValidException exception) {
        return exception.getBindingResult().getFieldErrors().stream().map(toVddFieldError()).toList();
    }

    private Function<FieldError, VddFieldError> toVddFieldError() {
        return error ->
            VddFieldError
                .builder()
                .fieldPath(error.getField())
                .reason(error.getDefaultMessage())
                .message(getMessage(error.getDefaultMessage(), buildArguments(extractSource(error))))
                .build();
    }

    private Function<ConstraintViolation<?>, VddFieldError> toFieldError() {
        return violation -> {
            Map<String, Object> arguments = buildArguments(violation);

            String message = violation.getMessage();
            return VddFieldError
                .builder()
                .fieldPath(violation.getPropertyPath().toString())
                .reason(message)
                .message(getMessage(message, arguments))
                .build();
        };
    }

    private Map<String, Object> buildArguments(ConstraintViolation<?> violation) {
        return violation
            .getConstraintDescriptor()
            .getAttributes()
            .entrySet()
            .stream()
            .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
        HttpMessageNotReadableException exception,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request
    ) {
        logger.error("Error reading query information: {}", exception.getMessage(), exception);

        VddError error = new VddError(DEFAULT_KEY, getMessage(DEFAULT_KEY, null), null);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<VddError> handleAuthenticationException(AuthenticationException exception) {
        logger.debug("A user tried to do an unauthorized operation: {}", exception.getMessage(), exception);

        VddError error = new VddError(NOT_AUTHENTICATED_KEY, getMessage(NOT_AUTHENTICATED_KEY, null), null);

        return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler
    public ResponseEntity<VddError> handleRuntimeException(Throwable throwable) {
        logger.error("An unhandled error occurs: {}", throwable.getMessage(), throwable);

        VddError error = new VddError(DEFAULT_KEY, getMessage(DEFAULT_KEY, null), null);
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
