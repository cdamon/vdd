package com.craft.vdd.error.infrastructure.primary;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Collection;
import java.util.List;

@Schema(description = "Error result for a WebService call")
public class VddError {

    @Schema(title = "Technical type of this error", example = "user.mandatory", required = true)
    private final String errorType;

    @Schema(
        title = "Human readable error message",
        example = "Une erreur technique est survenue lors du traitement de votre demande",
        required = true
    )
    private final String message;

    @Schema(title = "Invalid fields", required = false)
    private final List<VddFieldError> fieldsErrors;

    public VddError(
        @JsonProperty("errorType") String errorType,
        @JsonProperty("message") String message,
        @JsonProperty("fieldsErrors") List<VddFieldError> fieldsErrors
    ) {
        this.errorType = errorType;
        this.message = message;
        this.fieldsErrors = fieldsErrors;
    }

    public String getErrorType() {
        return errorType;
    }

    public String getMessage() {
        return message;
    }

    public Collection<VddFieldError> getFieldsErrors() {
        return fieldsErrors;
    }
}
