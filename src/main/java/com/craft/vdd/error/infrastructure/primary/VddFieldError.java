/**
 *
 */
package com.craft.vdd.error.infrastructure.primary;

import com.craft.vdd.error.infrastructure.primary.VddFieldError.VddFieldErrorBuilder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import io.swagger.v3.oas.annotations.media.Schema;

@JsonDeserialize(builder = VddFieldErrorBuilder.class)
@Schema(description = "Error for a field validation")
class VddFieldError {

    @Schema(title = "Path to the field in error", example = "address.country", required = true)
    private final String fieldPath;

    @Schema(title = "Technical reason for the invalidation", example = "user.mandatory", required = true)
    private final String reason;

    @Schema(title = "Human readable message for the invalidation", example = "Le champ doit être renseigné", required = true)
    private final String message;

    private VddFieldError(VddFieldErrorBuilder builder) {
        fieldPath = builder.fieldPath;
        reason = builder.reason;
        message = builder.message;
    }

    public static VddFieldErrorBuilder builder() {
        return new VddFieldErrorBuilder();
    }

    public String getFieldPath() {
        return fieldPath;
    }

    public String getReason() {
        return reason;
    }

    public String getMessage() {
        return message;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class VddFieldErrorBuilder {

        private String fieldPath;
        private String reason;
        private String message;

        public VddFieldErrorBuilder fieldPath(String fieldPath) {
            this.fieldPath = fieldPath;

            return this;
        }

        public VddFieldErrorBuilder reason(String reason) {
            this.reason = reason;

            return this;
        }

        public VddFieldErrorBuilder message(String message) {
            this.message = message;

            return this;
        }

        public VddFieldError build() {
            return new VddFieldError(this);
        }
    }
}
