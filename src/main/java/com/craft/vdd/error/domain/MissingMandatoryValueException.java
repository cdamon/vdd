package com.craft.vdd.error.domain;

public class MissingMandatoryValueException extends VddException {

    public MissingMandatoryValueException(String field) {
        super(
            VddException
                .builder(AssertErrorKey.MISSING_MANDATORY_VALUE)
                .message("Missing mandatory value in " + field)
                .argument("field", field)
        );
    }
}
