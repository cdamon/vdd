package com.craft.vdd.error.domain;

public interface ErrorKey {
    String key();
}
