package com.craft.vdd.error.domain;

import java.math.BigDecimal;
import java.util.Collection;

public final class Assert {

    private Assert() {}

    public static void notBlank(String field, String value) {
        Assert.notNull(field, value);

        if (value.isBlank()) {
            throw new MissingMandatoryValueException(field);
        }
    }

    public static void notEmpty(String field, Collection<?> value) {
        Assert.notNull(field, value);

        if (value.isEmpty()) {
            throw new MissingMandatoryValueException(field);
        }
    }

    public static void notNull(String field, Object value) {
        if (value == null) {
            throw new MissingMandatoryValueException(field);
        }
    }

    public static StringAsserter field(String field, String value) {
        return new StringAsserter(field, value);
    }

    public static DoubleAsserter field(String field, double value) {
        return new DoubleAsserter(field, value);
    }

    public static BigDecimalAsserter field(String field, BigDecimal value) {
        return new BigDecimalAsserter(field, value);
    }

    public static class StringAsserter {

        private final String field;
        private final String value;

        private StringAsserter(String field, String value) {
            this.field = field;
            this.value = value;
        }

        public StringAsserter notBlank() {
            Assert.notBlank(field, value);

            return this;
        }

        public StringAsserter maxLength(int maxLength) {
            if (value == null) {
                return this;
            }

            if (value.length() > maxLength) {
                throw StringTooLongException.builder().field(field).maxLength(maxLength).length(value.length()).build();
            }

            return this;
        }
    }

    public static class DoubleAsserter {

        private final String field;
        private final double value;

        public DoubleAsserter(String field, double value) {
            this.field = field;
            this.value = value;
        }

        public DoubleAsserter min(double min) {
            if (value < min) {
                throw InvalidNumberValueException.underMin(field).value(String.valueOf(value)).min(min).build();
            }

            return this;
        }

        public DoubleAsserter max(double max) {
            if (value > max) {
                throw InvalidNumberValueException.overMax(field).value(value).max(max).build();
            }

            return this;
        }
    }

    public static class BigDecimalAsserter {

        private final String field;
        private final BigDecimal value;

        private BigDecimalAsserter(String field, BigDecimal value) {
            this.field = field;
            this.value = value;
        }

        public BigDecimalAsserter notNull() {
            Assert.notNull(field, value);

            return this;
        }

        public BigDecimalAsserter positive() {
            if (value == null) {
                throw InvalidNumberValueException.underMin(field).value("null").min(0).build();
            }

            if (value.compareTo(BigDecimal.ZERO) < 0) {
                throw InvalidNumberValueException.underMin(field).value(value.toPlainString()).min(0).build();
            }

            return this;
        }
    }
}
