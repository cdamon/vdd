package com.craft.vdd.error.domain;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang3.StringUtils;

public class VddException extends RuntimeException {

    private static final String TECHNICAL_ERROR_MESSAGE = "A technical error occured";

    private final ErrorKey key;
    private final Map<String, Object> arguments;
    private final ErrorStatus status;

    public VddException(VddExceptionBuilder builder) {
        super(buildMessage(builder), buildCause(builder));
        Assert.notNull("builder", builder);

        key = buildKey(builder.key);
        arguments = builder.arguments;
        status = buildStatus(builder.status);
    }

    private static String buildMessage(VddExceptionBuilder builder) {
        if (builder == null || StringUtils.isBlank(builder.message)) {
            return TECHNICAL_ERROR_MESSAGE;
        }

        return builder.message;
    }

    private static Throwable buildCause(VddExceptionBuilder builder) {
        if (builder == null) {
            return null;
        }

        return builder.cause;
    }

    private ErrorKey buildKey(ErrorKey key) {
        if (key == null) {
            return StandardErrorKey.TECHNICAL_ERROR;
        }

        return key;
    }

    private ErrorStatus buildStatus(ErrorStatus status) {
        if (status == null) {
            return defaultStatus();
        }

        return status;
    }

    private ErrorStatus defaultStatus() {
        return Arrays
            .stream(Thread.currentThread().getStackTrace())
            .map(StackTraceElement::getClassName)
            .filter(name -> name.contains(".primary."))
            .findFirst()
            .map(stack -> ErrorStatus.BAD_REQUEST)
            .orElse(ErrorStatus.INTERNAL_SERVER_ERROR);
    }

    public static VddExceptionBuilder internalServerError(ErrorKey key) {
        return builder(key).status(ErrorStatus.INTERNAL_SERVER_ERROR);
    }

    public static VddExceptionBuilder badRequest(ErrorKey key) {
        return builder(key).status(ErrorStatus.BAD_REQUEST);
    }

    public static VddExceptionBuilder notFound(ErrorKey key) {
        return builder(key).status(ErrorStatus.NOT_FOUND);
    }

    public static VddExceptionBuilder unauthorized(ErrorKey key) {
        return builder(key).status(ErrorStatus.UNAUTHORIZED);
    }

    public static VddExceptionBuilder builder(ErrorKey key) {
        return new VddExceptionBuilder(key);
    }

    public ErrorKey key() {
        return key;
    }

    public Throwable cause() {
        return getCause();
    }

    public String message() {
        return getMessage();
    }

    public Map<String, Object> arguments() {
        return arguments;
    }

    public ErrorStatus status() {
        return status;
    }

    public static class VddExceptionBuilder {

        private final ErrorKey key;
        private final Map<String, Object> arguments = new ConcurrentHashMap<>();

        private String message;
        private Throwable cause;
        private ErrorStatus status;

        public VddExceptionBuilder(ErrorKey key) {
            this.key = key;
        }

        public VddExceptionBuilder message(String message) {
            this.message = message;

            return this;
        }

        public VddExceptionBuilder cause(Throwable cause) {
            this.cause = cause;

            return this;
        }

        public VddExceptionBuilder status(ErrorStatus status) {
            this.status = status;

            return this;
        }

        public VddExceptionBuilder argument(String key, Object value) {
            if (key == null) {
                return this;
            }

            arguments.put(key, value);

            return this;
        }

        public VddException build() {
            return new VddException(this);
        }
    }
}
