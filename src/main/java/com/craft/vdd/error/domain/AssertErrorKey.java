package com.craft.vdd.error.domain;

public enum AssertErrorKey implements ErrorKey {
    MISSING_MANDATORY_VALUE("missing-mandatory-value"),
    VALUE_UNDER_MIN("value-under-min"),
    VALUE_OVER_MAX("value-over-max"),
    STRING_TOO_LONG("string-too-long");

    private final String key;

    private AssertErrorKey(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return key;
    }
}
