package com.craft.vdd.error.domain;

public class InvalidNumberValueException extends VddException {

    private InvalidNumberValueException(ValueUnderMinExceptionBuilder builder) {
        this(
            VddException
                .builder(AssertErrorKey.VALUE_UNDER_MIN)
                .message("Value of field " + builder.field + " must be over " + builder.min + " but was " + builder.value)
                .argument("field", builder.field)
                .argument("value", builder.value)
                .argument("min", builder.min)
        );
    }

    private InvalidNumberValueException(ValueOverMaxExceptionBuilder builder) {
        this(
            VddException
                .builder(AssertErrorKey.VALUE_OVER_MAX)
                .message("Value of field " + builder.field + " must be under " + builder.max + " but was " + builder.value)
                .argument("field", builder.field)
                .argument("value", builder.value)
                .argument("max", builder.max)
        );
    }

    private InvalidNumberValueException(VddExceptionBuilder builder) {
        super(builder);
    }

    public static ValueUnderMinExceptionBuilder underMin(String field) {
        return new ValueUnderMinExceptionBuilder(field);
    }

    public static ValueOverMaxExceptionBuilder overMax(String field) {
        return new ValueOverMaxExceptionBuilder(field);
    }

    public static class ValueUnderMinExceptionBuilder {

        private final String field;
        private String value;
        private double min;

        public ValueUnderMinExceptionBuilder(String field) {
            this.field = field;
        }

        public ValueUnderMinExceptionBuilder value(String value) {
            this.value = value;

            return this;
        }

        public ValueUnderMinExceptionBuilder min(double min) {
            this.min = min;

            return this;
        }

        public InvalidNumberValueException build() {
            return new InvalidNumberValueException(this);
        }
    }

    public static class ValueOverMaxExceptionBuilder {

        private final String field;
        private double value;
        private double max;

        public ValueOverMaxExceptionBuilder(String field) {
            this.field = field;
        }

        public ValueOverMaxExceptionBuilder value(double value) {
            this.value = value;

            return this;
        }

        public ValueOverMaxExceptionBuilder max(double max) {
            this.max = max;

            return this;
        }

        public InvalidNumberValueException build() {
            return new InvalidNumberValueException(this);
        }
    }
}
