package com.craft.vdd.error.domain;

public class StringTooLongException extends VddException {

    private StringTooLongException(StringTooLongExceptionBuilder builder) {
        super(
            VddException
                .builder(AssertErrorKey.STRING_TOO_LONG)
                .message("Value in " + builder.field + " was " + builder.length + " but max length is " + builder.maxLength)
                .argument("field", builder.field)
                .argument("length", builder.length)
                .argument("maxLength", builder.maxLength)
        );
    }

    public static StringTooLongExceptionBuilder builder() {
        return new StringTooLongExceptionBuilder();
    }

    public static class StringTooLongExceptionBuilder {

        private String field;
        private int maxLength;
        private int length;

        public StringTooLongExceptionBuilder field(String field) {
            this.field = field;

            return this;
        }

        public StringTooLongExceptionBuilder maxLength(int maxLength) {
            this.maxLength = maxLength;

            return this;
        }

        public StringTooLongExceptionBuilder length(int length) {
            this.length = length;

            return this;
        }

        public StringTooLongException build() {
            return new StringTooLongException(this);
        }
    }
}
