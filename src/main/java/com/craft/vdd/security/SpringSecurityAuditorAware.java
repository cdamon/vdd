package com.craft.vdd.security;

import com.craft.vdd.GeneratedByJHipster;
import com.craft.vdd.authentication.infrastructure.primary.SecurityUtils;
import com.craft.vdd.config.Constants;
import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

/**
 * Implementation of {@link AuditorAware} based on Spring Security.
 */
@Component
@GeneratedByJHipster
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(SecurityUtils.getOptionalUserLogin().orElse(Constants.SYSTEM));
    }
}
