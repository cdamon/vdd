package com.craft.vdd.account.infrastructure.secondary;

import com.craft.vdd.account.domain.Account;
import com.craft.vdd.account.domain.AccountsRepository;
import org.springframework.stereotype.Repository;

@Repository
class VDDAccountsRepository implements AccountsRepository {

    private final TokensReader tokens;

    public VDDAccountsRepository(TokensReader tokens) {
        this.tokens = tokens;
    }

    @Override
    public Account authenticatedUser() {
        return tokens.read();
    }
}
