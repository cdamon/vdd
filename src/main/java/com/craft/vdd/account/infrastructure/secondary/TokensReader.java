package com.craft.vdd.account.infrastructure.secondary;

import com.craft.vdd.account.domain.Account;
import com.craft.vdd.authentication.infrastructure.primary.NotAuthenticatedUserException;
import com.craft.vdd.authentication.infrastructure.primary.SecurityUtils;
import java.util.Map;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
class TokensReader {

    public Account read() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        assertAuthenticated(authentication);

        Map<String, Object> attributes = readAttributes(authentication);

        return Account
            .builder()
            .username(SecurityUtils.getConnectedUserLogin())
            .email((String) attributes.get("email"))
            .firstname((String) attributes.get("given_name"))
            .lastname((String) attributes.get("family_name"))
            .authorities(authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList())
            .build();
    }

    private void assertAuthenticated(Authentication authentication) {
        if (authentication == null) {
            throw new NotAuthenticatedUserException();
        }
    }

    private Map<String, Object> readAttributes(Authentication authentication) {
        if (authentication instanceof JwtAuthenticationToken jwtToken) {
            return jwtToken.getTokenAttributes();
        }

        if (authentication instanceof OAuth2AuthenticationToken oauthToken) {
            return oauthToken.getPrincipal().getAttributes();
        }

        throw new NotAuthenticatedUserException();
    }
}
