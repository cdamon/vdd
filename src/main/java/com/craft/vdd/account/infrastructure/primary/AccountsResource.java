package com.craft.vdd.account.infrastructure.primary;

import com.craft.vdd.account.application.AccountsApplicationService;
import com.craft.vdd.account.domain.Account;
import com.craft.vdd.common.infrastructure.primary.VddApis;
import com.craft.vdd.error.infrastructure.primary.VddError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = VddApis.ACCOUNTS_TAG)
class AccountsResource {

    private final AccountsApplicationService accounts;

    public AccountsResource(AccountsApplicationService accounts) {
        this.accounts = accounts;
    }

    @GetMapping("/api/current-account")
    @Operation(summary = "Get the connected user account information")
    @ApiResponses(
        {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = RestUser.class))),
            @ApiResponse(
                responseCode = "500",
                content = @Content(schema = @Schema(implementation = VddError.class)),
                description = "An error occured while reading the account information"
            ),
        }
    )
    public ResponseEntity<RestUser> getConnectedAccount() {
        Account authenticatedUser = accounts.notSecured().authenticatedUser();
        return ResponseEntity.ok(RestUser.from(authenticatedUser));
    }
}
