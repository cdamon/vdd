package com.craft.vdd.account.application;

import com.craft.vdd.account.domain.Account;
import com.craft.vdd.account.domain.AccountsRepository;
import com.craft.vdd.common.application.NotSecured;
import org.springframework.stereotype.Service;

@Service
public class AccountsApplicationService {

    private final AccountsRepository accounts;
    private final NotSecuredAccountsApplicationService notSecured;

    public AccountsApplicationService(AccountsRepository accounts) {
        this.accounts = accounts;

        notSecured = new NotSecuredAccountsApplicationService();
    }

    @NotSecured
    public NotSecuredAccountsApplicationService notSecured() {
        return notSecured;
    }

    public class NotSecuredAccountsApplicationService {

        public Account authenticatedUser() {
            return accounts.authenticatedUser();
        }
    }
}
