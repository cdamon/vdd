package com.craft.vdd.account.domain;

import com.craft.vdd.authentication.domain.Username;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Account {

    private final Username username;
    private final Email email;
    private final Name name;
    private final Collection<Authority> authorities;
    private final boolean enabled;

    private Account(AccountBuilder builder) {
        username = new Username(builder.username);
        email = new Email(builder.email);
        name = new Name(builder.firstname, builder.lastname);
        authorities = buildAuthorities(builder.authorities);
        enabled = builder.enabled;
    }

    private Collection<Authority> buildAuthorities(Collection<String> authorities) {
        if (authorities == null) {
            return List.of();
        }

        return authorities.stream().map(Authority::new).collect(Collectors.toUnmodifiableSet());
    }

    public static AccountBuilder builder() {
        return new AccountBuilder();
    }

    public Username username() {
        return username;
    }

    public Email email() {
        return email;
    }

    public Firstname firstname() {
        return name.firstname();
    }

    public Lastname lastname() {
        return name.lastname();
    }

    public Collection<Authority> authorities() {
        return authorities;
    }

    public boolean enabled() {
        return enabled;
    }

    public static class AccountBuilder {

        private String username;
        private String email;
        private String firstname;
        private String lastname;
        private Collection<String> authorities;
        private boolean enabled;

        public AccountBuilder username(String username) {
            this.username = username;

            return this;
        }

        public AccountBuilder email(String email) {
            this.email = email;

            return this;
        }

        public AccountBuilder firstname(String firstname) {
            this.firstname = firstname;

            return this;
        }

        public AccountBuilder lastname(String lastname) {
            this.lastname = lastname;

            return this;
        }

        public AccountBuilder authorities(Collection<String> authorities) {
            this.authorities = authorities;

            return this;
        }

        public AccountBuilder enabled(boolean enabled) {
            this.enabled = enabled;

            return this;
        }

        public Account build() {
            return new Account(this);
        }
    }
}
