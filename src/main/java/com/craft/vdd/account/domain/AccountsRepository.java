package com.craft.vdd.account.domain;

public interface AccountsRepository {
    Account authenticatedUser();
}
