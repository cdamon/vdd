package com.craft.vdd.account.domain;

import com.craft.vdd.error.domain.Assert;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

public record Email(String email) {
    public Email(String email) {
        Assert.field("email", email).notBlank().maxLength(255);

        this.email = email;
    }

    public static Optional<Email> of(String email) {
        return Optional.ofNullable(email).filter(StringUtils::isNotBlank).map(Email::new);
    }

    public String get() {
        return email();
    }
}
