package com.craft.vdd.account.domain;

import com.craft.vdd.error.domain.Assert;

public record Authority(String authority) {
    public Authority {
        Assert.notBlank("authority", authority);
    }

    public String get() {
        return authority();
    }
}
