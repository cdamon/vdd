package com.craft.vdd.config;

import com.craft.vdd.GeneratedByJHipster;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Vdd.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link tech.jhipster.config.JHipsterProperties} for a good example.
 */
@GeneratedByJHipster
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {}
