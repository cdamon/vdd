package com.craft.vdd.authentication.infrastructure.primary;

import com.craft.vdd.error.domain.VddException;

public class NotAuthenticatedUserException extends VddException {

    public NotAuthenticatedUserException() {
        super(VddException.unauthorized(AuthenticationErrorKey.NOT_AUTHENTICATED).message("User is not authenticated"));
    }
}
