package com.craft.vdd.authentication.domain;

import com.craft.vdd.error.domain.Assert;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

public record Username(String username) implements Comparable<Username> {
    public Username {
        Assert.field("username", username).notBlank().maxLength(100);
    }

    public static Optional<Username> of(String username) {
        return Optional.ofNullable(username).filter(StringUtils::isNotBlank).map(Username::new);
    }

    public String get() {
        return username();
    }

    @Override
    public String toString() {
        return username();
    }

    @Override
    public int compareTo(Username other) {
        if (other == null) {
            return -1;
        }

        return username.compareTo(other.username);
    }
}
