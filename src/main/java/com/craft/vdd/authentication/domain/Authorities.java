package com.craft.vdd.authentication.domain;

import com.craft.vdd.GeneratedByJHipster;

@GeneratedByJHipster
public final class Authorities {

    public static final String ADMIN = "ROLE_ADMIN";
    public static final String USER = "ROLE_USER";

    private Authorities() {}
}
