package com.craft.vdd.common.application;

import com.craft.vdd.kipe.AccessChecker;
import com.craft.vdd.kipe.Resource;
import com.craft.vdd.kipe.VddAuthorizations;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
class ResourceAccessChecker implements AccessChecker<String> {

    @Override
    public boolean can(Authentication authentication, String action, String resource) {
        return VddAuthorizations.allAuthorized(authentication, action, Resource.valueOf(resource));
    }
}
