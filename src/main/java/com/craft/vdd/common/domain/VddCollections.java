package com.craft.vdd.common.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public final class VddCollections {

    private VddCollections() {}

    public static <K, V> Map<K, V> immutable(Map<K, V> map) {
        if (map == null) {
            return Map.of();
        }

        return Collections.unmodifiableMap(map);
    }

    public static <T> List<T> immutable(List<T> list) {
        if (list == null) {
            return List.of();
        }

        return Collections.unmodifiableList(list);
    }

    public static <T> Collection<T> immutable(Collection<T> collection) {
        if (collection == null) {
            return List.of();
        }

        return Collections.unmodifiableCollection(collection);
    }

    public static <T> Set<T> immutable(Set<T> collection) {
        if (collection == null) {
            return Set.of();
        }

        return Collections.unmodifiableSet(collection);
    }

    public static <T> List<T> mutable(List<T> list) {
        if (list == null) {
            return new ArrayList<>();
        }

        return new ArrayList<>(list);
    }

    public static <T> Set<T> mutable(Collection<T> set) {
        if (set == null) {
            return new HashSet<>();
        }

        return new HashSet<>(set);
    }

    public static <T> Stream<T> stream(Collection<T> collection) {
        if (collection == null) {
            return Stream.of();
        }

        return collection.stream();
    }

    public static <T> Stream<T> streamOf(T value) {
        if (value == null) {
            return Stream.of();
        }

        return Stream.of(value);
    }
}
