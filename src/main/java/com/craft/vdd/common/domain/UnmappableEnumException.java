package com.craft.vdd.common.domain;

import com.craft.vdd.error.domain.StandardErrorKey;
import com.craft.vdd.error.domain.VddException;

class UnmappableEnumException extends VddException {

    public <T extends Enum<T>, U extends Enum<U>> UnmappableEnumException(Class<T> from, Class<U> to) {
        super(VddException.internalServerError(StandardErrorKey.TECHNICAL_ERROR).message("Can't map " + from + " to " + to));
    }
}
