package com.craft.vdd.common.infrastructure.primary;

public final class ValidationMessage {

    public static final String MISSING_MANDATORY_VALUE = "user.missing-mandatory-value";
    public static final String WRONG_FORMAT = "user.wrong-format";
    public static final String VALUE_TOO_LOW = "user.too-low";
    public static final String VALUE_TOO_HIGH = "user.too-high";

    private ValidationMessage() {}
}
