package com.craft.vdd.pagination.domain;

import com.craft.vdd.common.domain.Generated;
import com.craft.vdd.error.domain.Assert;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class VddPageable {

    private final int page;
    private final int pageSize;
    private final int offset;

    public VddPageable(int page, int pageSize) {
        Assert.field("page", page).min(0);
        Assert.field("pageSize", pageSize).min(1).max(100);

        this.page = page;
        this.pageSize = pageSize;
        offset = page * pageSize;
    }

    public int page() {
        return page;
    }

    public int pageSize() {
        return pageSize;
    }

    public int offset() {
        return offset;
    }

    @Override
    @Generated
    public int hashCode() {
        return new HashCodeBuilder().append(page).append(pageSize).build();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        VddPageable other = (VddPageable) obj;
        return new EqualsBuilder().append(page, other.page).append(pageSize, other.pageSize).build();
    }
}
