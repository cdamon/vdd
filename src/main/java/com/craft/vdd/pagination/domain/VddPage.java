package com.craft.vdd.pagination.domain;

import com.craft.vdd.error.domain.Assert;
import java.util.List;
import java.util.function.Function;

public class VddPage<T> {

    private static final int MINIMAL_PAGE_COUNT = 1;
    private final List<T> content;
    private final int currentPage;
    private final int pageSize;
    private final long totalElementsCount;

    private VddPage(VddPageBuilder<T> builder) {
        content = buildContent(builder.content);
        currentPage = builder.currentPage;
        pageSize = buildPageSize(builder.pageSize);
        totalElementsCount = buildTotalElementsCount(builder.totalElementsCount);
    }

    private List<T> buildContent(List<T> content) {
        if (content == null) {
            return List.of();
        }

        return content;
    }

    private int buildPageSize(int pageSize) {
        if (pageSize == -1) {
            return content.size();
        }

        return pageSize;
    }

    private long buildTotalElementsCount(long totalElementsCount) {
        if (totalElementsCount == -1) {
            return content.size();
        }

        return totalElementsCount;
    }

    public static <T> VddPageBuilder<T> builder(List<T> content) {
        return new VddPageBuilder<>(content);
    }

    public static <T> VddPage<T> singlePage(List<T> content) {
        return builder(content).build();
    }

    public List<T> content() {
        return content;
    }

    public int currentPage() {
        return currentPage;
    }

    public int pageSize() {
        return pageSize;
    }

    public long totalElementsCount() {
        return totalElementsCount;
    }

    public int pageCount() {
        if (totalElementsCount > 0) {
            return (int) Math.ceil(totalElementsCount / (float) pageSize);
        }

        return MINIMAL_PAGE_COUNT;
    }

    public boolean isNotLast() {
        return currentPage + 1 != pageCount();
    }

    public <R> VddPage<R> map(Function<T, R> mapper) {
        Assert.notNull("mapper", mapper);

        return builder(content().stream().map(mapper).toList())
            .currentPage(currentPage)
            .pageSize(pageSize)
            .totalElementsCount(totalElementsCount)
            .build();
    }

    public static class VddPageBuilder<T> {

        private final List<T> content;
        private int currentPage;
        private int pageSize = -1;
        private long totalElementsCount = -1;

        private VddPageBuilder(List<T> content) {
            this.content = content;
        }

        public VddPageBuilder<T> pageSize(int pageSize) {
            this.pageSize = pageSize;

            return this;
        }

        public VddPageBuilder<T> currentPage(int currentPage) {
            this.currentPage = currentPage;

            return this;
        }

        public VddPageBuilder<T> totalElementsCount(long totalElementsCount) {
            this.totalElementsCount = totalElementsCount;

            return this;
        }

        public VddPage<T> build() {
            return new VddPage<>(this);
        }
    }
}
