package com.craft.vdd.pagination.infrastructure.primary;

import com.craft.vdd.error.domain.Assert;
import com.craft.vdd.pagination.domain.VddPage;
import com.craft.vdd.pagination.domain.VddPageable;
import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import java.util.function.Function;

@Schema(name = "Page", description = "Paginated content")
public class RestVddPage<T> {

    @Schema(title = "Page content")
    private final List<T> content;

    @Schema(title = "Current page (starts at 0)", required = true)
    private final int currentPage;

    @Schema(title = "Number of elements on each page", required = true)
    private final int pageSize;

    @Schema(title = "Total number of elements to paginate", required = true)
    private final long totalElementsCount;

    @Schema(title = "Number of resulting pages", required = true)
    private final int pagesCount;

    private RestVddPage(RestVddPageBuilder<T> builder) {
        content = builder.content;
        currentPage = builder.currentPage;
        pageSize = builder.pageSize;
        totalElementsCount = builder.totalElementsCount;
        pagesCount = builder.pageCount;
    }

    public static <S, T> RestVddPage<T> from(VddPage<S> source, Function<S, T> mapper) {
        Assert.notNull("source", source);
        Assert.notNull("mapper", mapper);

        return new RestVddPageBuilder<>(source.content().parallelStream().map(mapper).toList())
            .currentPage(source.currentPage())
            .pageSize(source.pageSize())
            .totalElementsCount(source.totalElementsCount())
            .pageCount(source.pageCount())
            .build();
    }

    public static <S, T> RestVddPage<T> from(List<S> source, RestVddPageable pagination, Function<S, T> mapper) {
        Assert.notNull("source", source);
        Assert.notNull("pagination", pagination);
        Assert.notNull("mapper", mapper);

        VddPageable pageable = pagination.toPageable();

        return new RestVddPageBuilder<>(
            source
                .subList(pageable.offset(), Math.min(source.size(), pageable.offset() + pageable.pageSize()))
                .stream()
                .map(mapper)
                .toList()
        )
            .currentPage(pageable.page())
            .totalElementsCount(source.size())
            .pageCount((int) Math.ceil(source.size() / (float) pageable.pageSize()))
            .pageSize(pageable.pageSize())
            .build();
    }

    public List<T> getContent() {
        return content;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public long getTotalElementsCount() {
        return totalElementsCount;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    private static class RestVddPageBuilder<T> {

        private final List<T> content;
        private int currentPage;
        private int pageSize;
        private long totalElementsCount;
        private int pageCount;

        private RestVddPageBuilder(List<T> content) {
            this.content = content;
        }

        public RestVddPageBuilder<T> pageSize(int pageSize) {
            this.pageSize = pageSize;

            return this;
        }

        public RestVddPageBuilder<T> currentPage(int currentPage) {
            this.currentPage = currentPage;

            return this;
        }

        public RestVddPageBuilder<T> totalElementsCount(long totalElementsCount) {
            this.totalElementsCount = totalElementsCount;

            return this;
        }

        public RestVddPageBuilder<T> pageCount(int pageCount) {
            this.pageCount = pageCount;

            return this;
        }

        public RestVddPage<T> build() {
            return new RestVddPage<>(this);
        }
    }
}
