package com.craft.vdd.pagination.infrastructure.secondary;

import com.craft.vdd.error.domain.Assert;
import com.craft.vdd.pagination.domain.VddPage;
import com.craft.vdd.pagination.domain.VddPageable;
import java.util.function.Function;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public final class VddPages {

    private VddPages() {}

    public static Pageable from(VddPageable pagination) {
        return from(pagination, Sort.unsorted());
    }

    public static Pageable from(VddPageable pagination, Sort sort) {
        Assert.notNull("pagination", pagination);
        Assert.notNull("sort", sort);

        return PageRequest.of(pagination.page(), pagination.pageSize(), sort);
    }

    public static <S, T> VddPage<T> from(Page<S> springPage, Function<S, T> mapper) {
        Assert.notNull("springPage", springPage);
        Assert.notNull("mapper", mapper);

        return VddPage
            .builder(springPage.getContent().parallelStream().map(mapper).toList())
            .currentPage(springPage.getNumber())
            .pageSize(springPage.getSize())
            .totalElementsCount(springPage.getTotalElements())
            .build();
    }
}
