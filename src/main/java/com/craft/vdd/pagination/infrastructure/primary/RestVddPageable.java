package com.craft.vdd.pagination.infrastructure.primary;

import com.craft.vdd.common.domain.Generated;
import com.craft.vdd.common.infrastructure.primary.ValidationMessage;
import com.craft.vdd.pagination.domain.VddPageable;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Schema(name = "VddPageable", description = "Pagination information")
public class RestVddPageable {

    @Schema(title = "Page to display (starts at 0)", example = "0")
    private int page;

    @Schema(title = "Number of elements on each page", example = "10")
    private int pageSize = 10;

    @Generated
    public RestVddPageable() {}

    public RestVddPageable(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    @Min(message = ValidationMessage.VALUE_TOO_LOW, value = 0)
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Min(message = ValidationMessage.VALUE_TOO_LOW, value = 1)
    @Max(message = ValidationMessage.VALUE_TOO_HIGH, value = 100)
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public VddPageable toPageable() {
        return new VddPageable(page, pageSize);
    }
}
