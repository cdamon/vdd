package com.craft.vdd.kipe;

import com.craft.vdd.config.SecurityConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

@AutoConfigureBefore(SecurityConfiguration.class)
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
class KipeConfiguration extends GlobalMethodSecurityConfiguration {

    private final AccessEvaluator evaluator;

    // All beans here need to be Lazy otherwise it'll break AOP (cache, transactions, etc...)
    public KipeConfiguration(@Lazy AccessEvaluator evaluator) {
        this.evaluator = evaluator;
    }

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        return new KipeMethodSecurityExpressionHandler(evaluator);
    }
}
