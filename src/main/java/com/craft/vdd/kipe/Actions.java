package com.craft.vdd.kipe;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class Actions {

    private final Set<Action> actions;

    public Actions(Action... actions) {
        this.actions = buildActions(actions);
    }

    static ActionsBuilder builder() {
        return new ActionsBuilder();
    }

    private Set<Action> buildActions(Action... actions) {
        if (actions == null) {
            return Set.of();
        }

        return Set.of(actions);
    }

    public boolean allAuthorized(String action, Resource resource) {
        return actions.contains(Action.all(action, resource));
    }

    public boolean specificAuthorized(String action, Resource resource) {
        return actions.contains(Action.specific(action, resource)) || allAuthorized(action, resource);
    }

    static class ActionsBuilder {

        private final List<Action> actions = new ArrayList<>();

        ActionsBuilder all(String action, Resource resource) {
            actions.add(Action.all(action, resource));

            return this;
        }

        ActionsBuilder specific(String action, Resource resource) {
            actions.add(Action.specific(action, resource));

            return this;
        }

        Actions build() {
            return new Actions(actions.toArray(Action[]::new));
        }
    }
}
