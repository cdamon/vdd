package com.craft.vdd.kipe;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

class KipeMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {

    private final AccessEvaluator evaluator;

    KipeMethodSecurityExpressionHandler(AccessEvaluator evaluator) {
        this.evaluator = evaluator;
    }

    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication, MethodInvocation invocation) {
        KipeMethodSecurityExpressionRoot root = new KipeMethodSecurityExpressionRoot(authentication, evaluator);

        root.setThis(invocation.getThis());
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(getTrustResolver());
        root.setRoleHierarchy(getRoleHierarchy());
        root.setDefaultRolePrefix(getDefaultRolePrefix());

        return root;
    }
}
