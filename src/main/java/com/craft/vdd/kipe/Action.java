package com.craft.vdd.kipe;

import com.craft.vdd.error.domain.Assert;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

class Action {

    private final String action;
    private final Resource resource;
    private final boolean allAuthorized;

    public Action(String action, Resource resource, boolean allAuthorized) {
        Assert.notBlank("action", action);
        Assert.notNull("resource", resource);

        this.action = action;
        this.resource = resource;
        this.allAuthorized = allAuthorized;
    }

    public static Action all(String action, Resource resource) {
        return new Action(action, resource, true);
    }

    public static Action specific(String action, Resource resource) {
        return new Action(action, resource, false);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(action).append(resource).append(allAuthorized).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Action other = (Action) obj;
        return new EqualsBuilder()
            .append(action, other.action)
            .append(resource, other.resource)
            .append(allAuthorized, other.allAuthorized)
            .build();
    }
}
