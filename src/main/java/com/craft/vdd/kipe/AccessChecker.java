package com.craft.vdd.kipe;

import org.springframework.security.core.Authentication;

/**
 * Check accesses for a given resource type
 *
 * @param <T>
 *          type of resources to check access for
 */
@FunctionalInterface
public interface AccessChecker<T> {
    /**
     * Check if the authenticated user can access the item
     *
     * @param authentication
     *          authenticated user information
     * @param action
     *          action to check
     * @param item
     *          element to check action possibility on to
     * @return true is the user can do the action on the resource, false otherwise
     */
    boolean can(Authentication authentication, String action, T item);

    @SuppressWarnings("unchecked")
    default boolean canOnObject(Authentication authentication, String action, Object item) {
        return can(authentication, action, (T) item);
    }
}
