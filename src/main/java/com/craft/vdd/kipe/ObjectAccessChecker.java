package com.craft.vdd.kipe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
class ObjectAccessChecker implements AccessChecker<Object> {

    private static final Logger logger = LoggerFactory.getLogger(ObjectAccessChecker.class);

    @Override
    public boolean can(Authentication authentication, String action, Object item) {
        logger.error("Error checking rights, falled back to default handler for action {} on class {}", action, getItemClass(item));

        return false;
    }

    private String getItemClass(Object item) {
        if (item == null) {
            return "unknown";
        }

        return item.getClass().getName();
    }
}
