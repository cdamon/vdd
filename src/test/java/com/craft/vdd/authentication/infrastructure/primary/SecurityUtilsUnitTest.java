package com.craft.vdd.authentication.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.craft.vdd.authentication.domain.Username;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

public class SecurityUtilsUnitTest {

    @BeforeEach
    void resetAuthentication() {
        SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
    }

    @Test
    void shouldNotGetUsernameForNotAuthenticatedUser() {
        assertThatThrownBy(() -> SecurityUtils.getUsername()).isExactlyInstanceOf(NotAuthenticatedUserException.class);
    }

    @Test
    void shouldGetUsername() {
        mockUserDetailsAuthentication();

        assertThat(SecurityUtils.getUsername()).isEqualTo(new Username("vdd"));
    }

    @Test
    void shouldGetEmptyUsernameForNotAuthenticatedUser() {
        assertThat(SecurityUtils.getOptionalUsername()).isEmpty();
    }

    @Test
    void shouldGetAuthenticatedUserUsername() {
        mockUserDetailsAuthentication();

        assertThat(SecurityUtils.getOptionalUsername()).contains(new Username("vdd"));
    }

    @Test
    void shouldNotGetCurrentUserLoginForNotAuthenticatedUser() {
        assertThatThrownBy(() -> SecurityUtils.getConnectedUserLogin()).isExactlyInstanceOf(NotAuthenticatedUserException.class);
    }

    @Test
    void shouldNotGetCurrentUserLoginForDummyAuthenticatedUser() {
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(mock(Authentication.class));
        SecurityContextHolder.setContext(securityContext);

        assertThatThrownBy(() -> SecurityUtils.getConnectedUserLogin()).isExactlyInstanceOf(NotAuthenticatedUserException.class);
    }

    @Test
    void shouldGetAuthenticatedUserFromUserDetails() {
        mockUserDetailsAuthentication();

        assertThat(SecurityUtils.getConnectedUserLogin()).isEqualTo("vdd");
    }

    private void mockUserDetailsAuthentication() {
        UserDetails details = mock(UserDetails.class);
        when(details.getUsername()).thenReturn("vdd");

        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(details);

        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void shouldGetAuthenticatedFromJwtAuthentication() {
        Jwt jwt = mock(Jwt.class);
        when(jwt.getClaims()).thenReturn(Map.of("preferred_username", "vdd"));

        JwtAuthenticationToken authentication = new JwtAuthenticationToken(jwt);

        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        assertThat(SecurityUtils.getConnectedUserLogin()).isEqualTo("vdd");
    }

    @Test
    void shouldGetAuthenticatedUserFromOidc() {
        DefaultOidcUser oidc = mock(DefaultOidcUser.class);
        when(oidc.getPreferredUsername()).thenReturn("vdd");

        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(oidc);

        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        assertThat(SecurityUtils.getConnectedUserLogin()).isEqualTo("vdd");
    }

    @Test
    void shouldGetAuthenticatedUserFromOAuth2() {
        OAuth2User user = mock(OAuth2User.class);
        when(user.getName()).thenReturn("vdd");

        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(user);

        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        assertThat(SecurityUtils.getConnectedUserLogin()).isEqualTo("vdd");
    }

    @Test
    void shouldGetAuthenticationFromString() {
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("vdd");

        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        assertThat(SecurityUtils.getConnectedUserLogin()).isEqualTo("vdd");
    }

    @Test
    void shouldGetEmptyAuthoritiesWithoutClaim() {
        assertThat(SecurityUtils.extractAuthorityFromClaims(null)).isEmpty();
    }

    @Test
    void shouldExtractAuthorityFromGroups() {
        assertThat(SecurityUtils.extractAuthorityFromClaims(Map.of("groups", List.of("ROLE_USER"))))
            .extracting(GrantedAuthority::getAuthority)
            .containsExactly("ROLE_USER");
    }

    @Test
    void shouldExtractAuthorityFromRoles() {
        assertThat(SecurityUtils.extractAuthorityFromClaims(Map.of("roles", List.of("ROLE_USER"))))
            .extracting(GrantedAuthority::getAuthority)
            .containsExactly("ROLE_USER");
    }

    @Test
    void shouldGetEmptyAuthoritiesWithoutAuthorities() {
        assertThat(SecurityUtils.extractAuthorityFromClaims(Map.of("dummy", List.of("ROLE_USER")))).isEmpty();
    }

    @Test
    void shouldGetEmptyRolesForNotAuthenticatedUser() {
        assertThat(SecurityUtils.getAuthenticatedUserRoles()).isEmpty();
    }

    @Test
    void shouldGetRolesFromAuthorities() {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        securityContext.setAuthentication(
            new UsernamePasswordAuthenticationToken(
                "vdd",
                "admin",
                List.of(new SimpleGrantedAuthority("ROLE_USER"), new SimpleGrantedAuthority("DUMMY"))
            )
        );
        SecurityContextHolder.setContext(securityContext);

        assertThat(SecurityUtils.getAuthenticatedUserRoles()).containsExactly("ROLE_USER");
    }

    public static void noAuthenticatedUser() {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();

        SecurityContextHolder.setContext(securityContext);
    }

    public static void mockAuthenticatedUser(String username, String... roles) {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        List<SimpleGrantedAuthority> userRoles = Arrays
            .stream(roles)
            .map(addRolePrefix())
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());

        securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(username, "admin", userRoles));

        SecurityContextHolder.setContext(securityContext);
    }

    private static Function<String, String> addRolePrefix() {
        return role -> {
            if (role.startsWith("ROLE_")) {
                return role;
            }

            return "ROLE_" + role;
        };
    }
}
