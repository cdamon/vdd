package com.craft.vdd.authentication.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.craft.vdd.authentication.domain.Authorities;
import com.craft.vdd.cucumber.CucumberTestContext;
import com.craft.vdd.cucumber.MockedSecurityContextRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

public class AuthenticationSteps {

    @Autowired
    private MockedSecurityContextRepository contexts;

    private static final Map<String, AuthenticatedUser> USERS = buildAuthentications();

    private static Map<String, AuthenticatedUser> buildAuthentications() {
        Map<String, AuthenticatedUser> users = new HashMap<>();

        users.put("user", user("user", Authorities.USER));
        users.put("admin", user("admin", Authorities.ADMIN));

        return users;
    }

    private static AuthenticatedUser user(String username, String profile) {
        return new AuthenticatedUser(token(username, profile));
    }

    private static JwtAuthenticationToken token(String username, String profile) {
        Jwt jwt = mock(Jwt.class);
        when(jwt.getClaims())
            .thenReturn(
                Map.of("preferred_username", username, "email", "email@vdd.com", "given_name", "Jean-Michel", "family_name", "DUPOND")
            );

        JwtAuthenticationToken token = new JwtAuthenticationToken(jwt, Stream.of(profile).map(SimpleGrantedAuthority::new).toList());
        token.setAuthenticated(true);

        return token;
    }

    @Given("I am logged out")
    public void logOut() {
        SecurityContextHolder.getContext().setAuthentication(null);

        contexts.authentication(null);
    }

    @Given("I am logged in as {string}")
    public void authenticateUser(String username) {
        AuthenticatedUser user = USERS.get(username);

        Authentication authentication = user.authentication;
        SecurityContextHolder.getContext().setAuthentication(authentication);

        contexts.authentication(authentication);
    }

    @Given("I logout")
    @Given("I am not logged in")
    public void logout() {
        contexts.authentication(null);
    }

    @Then("Access is forbidden")
    public void shouldGetAuthorizationError() {
        assertThat(CucumberTestContext.getStatus()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    private static class AuthenticatedUser {

        private final Authentication authentication;

        private AuthenticatedUser(Authentication authentication) {
            this.authentication = authentication;
        }
    }
}
