package com.craft.vdd.authentication.domain;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.domain.MissingMandatoryValueException;
import com.craft.vdd.error.domain.StringTooLongException;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

class UsernameUnitTest {

    @Test
    void shouldNotBuildWithoutUsername() {
        assertThatThrownBy(() -> new Username(null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("username");
    }

    @Test
    void shouldNotBuildWithBlankUsername() {
        assertThatThrownBy(() -> new Username(" "))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("username");
    }

    @Test
    void shouldNotBuildWithTooLongUsername() {
        assertThatThrownBy(() -> new Username("a".repeat(101)))
            .isExactlyInstanceOf(StringTooLongException.class)
            .hasMessageContaining("username");
    }

    @Test
    void shouldGetEmptyUsernameFromNullUsername() {
        assertThat(Username.of(null)).isEmpty();
    }

    @Test
    void shouldGetEmptyUsernameFromBlankUsername() {
        assertThat(Username.of(" ")).isEmpty();
    }

    @Test
    void shouldGetUsernameFromActualUsername() {
        assertThat(Username.of("user")).contains(new Username("user"));
    }

    @Test
    void shouldGetUsername() {
        Username username = new Username("user");

        assertThat(username.get()).isEqualTo("user");
    }

    @Test
    void shouldGetUsernameAsToString() {
        assertThat(username()).hasToString("bob");
    }

    @Test
    void shouldNotBeEqualToNull() {
        assertThat(username().equals(null)).isFalse();
    }

    @Test
    void shouldBeEqualToSelf() {
        Username username = username();

        assertThat(username.equals(username)).isTrue();
    }

    @Test
    @SuppressWarnings("unlikely-arg-type")
    void shouldNotBeEqualToAnotherType() {
        assertThat(username().equals("another")).isFalse();
    }

    @Test
    void shouldBeEqualToUsernameWithSameUsername() {
        assertThat(username().equals(username())).isTrue();
    }

    @Test
    void shouldNotBeEqualToUsernameWithAnotherUsername() {
        assertThat(username().equals(another())).isFalse();
    }

    @Test
    void shouldHaveSameHashCodeWithSameUsername() {
        assertThat(username()).hasSameHashCodeAs(username());
    }

    @Test
    void shouldHaveDifferentHashCodeWithDifferentUsername() {
        assertThat(username().hashCode()).isNotEqualTo(another().hashCode());
    }

    @Test
    void shouldSortUsernames() {
        List<Username> usernames = Arrays.asList(null, username(), another()).stream().sorted().toList();

        assertThat(usernames).containsExactly(another(), username(), null);
    }

    private Username username() {
        return new Username("bob");
    }

    private Username another() {
        return new Username("another");
    }
}
