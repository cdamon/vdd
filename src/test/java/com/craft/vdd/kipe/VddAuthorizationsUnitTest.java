package com.craft.vdd.kipe;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.authentication.domain.Authorities;
import com.craft.vdd.authentication.domain.Username;
import com.craft.vdd.authentication.infrastructure.primary.NotAuthenticatedUserException;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

class VddAuthorizationsUnitTest {

    @Test
    void shouldNotGetUsernameWithoutAuthentication() {
        assertThatThrownBy(() -> VddAuthorizations.getUsername(null)).isExactlyInstanceOf(NotAuthenticatedUserException.class);
    }

    @Test
    void shouldGetUsernameFromAuthentication() {
        assertThat(VddAuthorizations.getUsername(user())).isEqualTo(new Username("user"));
    }

    @Test
    void shouldNotBeAllAuthorizedWhenCheckingWithoutAuthentication() {
        assertThat(VddAuthorizations.allAuthorized(null, "action", Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeAllAuthorizedWhenCheckingWithoutAction() {
        assertThat(VddAuthorizations.allAuthorized(user(), null, Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeAllAuthorizedWhenCheckingWithBlankAction() {
        assertThat(VddAuthorizations.allAuthorized(user(), " ", Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeAllAuthorizedWhenCheckingWithoutResource() {
        assertThat(VddAuthorizations.allAuthorized(user(), "action", null)).isFalse();
    }

    @Test
    void shouldNotBeAllAuthorizedWhenCheckingForUnauthorizedAction() {
        assertThat(VddAuthorizations.allAuthorized(user(), "save", Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeAllAuthorizedWhenCheckingForSpecificAuthorizedAction() {
        assertThat(VddAuthorizations.allAuthorized(user(), "read", Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeAllAuthorizedWhenCheckingForAllUnauthorizedNoRoleAction() {
        assertThat(VddAuthorizations.allAuthorized(admin(), "write", Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeAllAuthorizedWhenCheckingForUnknownAction() {
        assertThat(VddAuthorizations.allAuthorized(admin(), "dummy", Resource.USERS)).isFalse();
    }

    @Test
    void shouldBeAllAuthorizedWhenCheckingForAllAuthorizedNoRoleAction() {
        assertThat(VddAuthorizations.allAuthorized(admin(), "list", Resource.USERS)).isTrue();
    }

    @Test
    void shouldBeAllAuthorizedWhenCheckingForAllAuthorizedAction() {
        assertThat(VddAuthorizations.allAuthorized(admin(), "read", Resource.MANAGERS)).isTrue();
    }

    @Test
    void shouldNotBeSpecificAuthorizedWhenCheckingWithoutAuthentication() {
        assertThat(VddAuthorizations.specificAuthorized(null, "action", Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeSpecificAuthorizedWhenCheckingWithoutAction() {
        assertThat(VddAuthorizations.specificAuthorized(user(), null, Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeSpecificAuthorizedWhenCheckingWithBlankAction() {
        assertThat(VddAuthorizations.specificAuthorized(user(), " ", Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeSpecificAuthorizedWhenCheckingWithoutResource() {
        assertThat(VddAuthorizations.specificAuthorized(user(), "action", null)).isFalse();
    }

    @Test
    void shouldNotBeSpecificAuthorizedWhenCheckingForUnauthorizedAction() {
        assertThat(VddAuthorizations.specificAuthorized(user(), "update", Resource.USERS)).isFalse();
    }

    @Test
    void shouldNotBeSpecificAuthorizedWhenCheckingForUnknownAction() {
        assertThat(VddAuthorizations.specificAuthorized(admin(), "dummy", Resource.USERS)).isFalse();
    }

    @Test
    void shouldBeSpecificAuthorizedWhenCheckingForSpecificAuthorizedNoRoleAction() {
        assertThat(VddAuthorizations.specificAuthorized(user(), "read", Resource.USERS)).isTrue();
    }

    @Test
    void shouldBeSpecificAuthorizedWhenCheckingForAllAuthorizedAction() {
        assertThat(VddAuthorizations.specificAuthorized(admin(), "read", Resource.MANAGERS)).isTrue();
    }

    private Authentication user() {
        return new TestingAuthenticationToken("user", null, Authorities.USER);
    }

    private Authentication admin() {
        return new TestingAuthenticationToken("gerard", null, Authorities.ADMIN);
    }
}
