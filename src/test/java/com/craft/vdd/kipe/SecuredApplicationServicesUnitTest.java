package com.craft.vdd.kipe;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.common.application.NotSecured;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

class SecuredApplicationServicesUnitTest {

    private static final Collection<String> OBJECT_METHODS = javaLangObjectMethods();

    private static List<String> javaLangObjectMethods() {
        return Arrays.stream(Object.class.getMethods()).map(Method::getName).collect(Collectors.toList());
    }

    @Test
    void shouldHaveOnlySecuredApplicationServiceMethods() {
        publicApplicationServiceMethods().forEach(assertHaveSecuredAnnotation());
    }

    private Set<Method> publicApplicationServiceMethods() {
        return new Reflections("com.craft.vdd")
            .getTypesAnnotatedWith(Service.class)
            .stream()
            .filter(service -> service.getName().endsWith("ApplicationService"))
            .flatMap(applicationService -> Arrays.stream(applicationService.getMethods()))
            .filter(method -> !Modifier.isPrivate(method.getModifiers()))
            .filter(method -> !OBJECT_METHODS.contains(method.getName()))
            .collect(Collectors.toSet());
    }

    private Consumer<Method> assertHaveSecuredAnnotation() {
        return method -> {
            List<Class<?>> annotationsClasses = Arrays
                .stream(method.getAnnotations())
                .map(Annotation::annotationType)
                .collect(Collectors.toList());

            assertThat(annotationsClasses)
                .withFailMessage("Didn't found secure annotation on <%s>", method)
                .containsAnyOf(PreAuthorize.class, PostAuthorize.class, NotSecured.class);
        };
    }
}
