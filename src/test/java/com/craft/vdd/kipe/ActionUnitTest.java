package com.craft.vdd.kipe;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.domain.MissingMandatoryValueException;
import org.junit.jupiter.api.Test;

class ActionUnitTest {

    @Test
    void shouldNotBuildAllActionWithoutAction() {
        assertThatThrownBy(() -> Action.all(null, Resource.USERS))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("action");
    }

    @Test
    void shouldNotBuildAllActionWithBlankAction() {
        assertThatThrownBy(() -> Action.all(" ", Resource.USERS))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("action");
    }

    @Test
    void shouldNotBuildAllActionWithoutResource() {
        assertThatThrownBy(() -> Action.all("action", null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("resource");
    }

    @Test
    void shouldNotBuildSpecificActionWithoutAction() {
        assertThatThrownBy(() -> Action.specific(null, Resource.USERS))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("action");
    }

    @Test
    void shouldNotBuildSpecificActionWithBlankAction() {
        assertThatThrownBy(() -> Action.specific(" ", Resource.USERS))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("action");
    }

    @Test
    void shouldNotBuildSpecificActionWithoutResource() {
        assertThatThrownBy(() -> Action.specific("action", null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("resource");
    }

    @Test
    void shouldNotBeEqualToNull() {
        assertThat(action().equals(null)).isFalse();
    }

    @Test
    @SuppressWarnings("unlikely-arg-type")
    void shouldNotBeEqualToAnotherType() {
        assertThat(action().equals("action")).isFalse();
    }

    @Test
    void shouldBeEqualToSelf() {
        Action action = action();

        assertThat(action.equals(action)).isTrue();
    }

    @Test
    void shouldNotBeEqualToActionWithAnotherAction() {
        assertThat(action().equals(Action.all("dummy", Resource.USERS))).isFalse();
    }

    @Test
    void shouldNotBeEqualToActionWithAnotherResource() {
        assertThat(action().equals(Action.all("action", Resource.MANAGERS))).isFalse();
    }

    @Test
    void shouldNotBeEqualToActionWithAnotherAuthorization() {
        assertThat(action().equals(Action.specific("action", Resource.USERS))).isFalse();
    }

    @Test
    void shouldBeEqualToActionWithSameActionResourceAndAuthorization() {
        assertThat(action().equals(action())).isTrue();
    }

    @Test
    void shouldGetSameHashCodeWithSameAction() {
        assertThat(action()).hasSameHashCodeAs(action());
    }

    @Test
    void shouldGetDifferentHashCodeWithDifferentAction() {
        assertThat(action().hashCode()).isNotEqualTo(Action.all("dummy", Resource.USERS).hashCode());
    }

    @Test
    void shouldGetDifferentHashCodeWithDifferentResource() {
        assertThat(action().hashCode()).isNotEqualTo(Action.all("action", Resource.MANAGERS).hashCode());
    }

    @Test
    void shouldGetDifferentHashCodeWithDifferentAuthorization() {
        assertThat(action().hashCode()).isNotEqualTo(Action.specific("action", Resource.USERS).hashCode());
    }

    private static Action action() {
        return Action.all("action", Resource.USERS);
    }
}
