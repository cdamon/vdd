package com.craft.vdd.kipe;

import static org.assertj.core.api.Assertions.*;

import ch.qos.logback.classic.Level;
import com.craft.vdd.LogSpy;
import com.craft.vdd.error.domain.MissingMandatoryValueException;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;

@ExtendWith(LogSpy.class)
class AccessEvaluatorUnitTest {

    @Mock
    private Authentication authentication;

    private final LogSpy logs;

    public AccessEvaluatorUnitTest(LogSpy logs) {
        this.logs = logs;
    }

    @Test
    void shouldNotBuildWithoutObjectChecker() {
        assertThatThrownBy(() -> new AccessEvaluator(List.of())).isExactlyInstanceOf(MissingMandatoryValueException.class);
    }

    @Test
    void shouldResolveOnDefaultCheckerForNullObject() {
        AccessEvaluator canEvaluator = new AccessEvaluator(List.of(new ObjectAccessChecker()));

        canEvaluator.can(authentication, "action", null);

        logs.assertLogged(Level.ERROR, "default handler");
    }

    @Test
    void shouldResolveOnDefaultCheckerForUnknownType() {
        AccessEvaluator canEvaluator = new AccessEvaluator(List.of(new ObjectAccessChecker()));

        canEvaluator.can(authentication, "action", "yo");

        logs.assertLogged(Level.ERROR, "default handler");
    }

    @Test
    void shouldGetMatchingEvaluator() {
        AccessEvaluator canEvaluator = new AccessEvaluator(List.of(new ObjectAccessChecker(), new DummyChecker()));

        assertThat(canEvaluator.can(authentication, "action", new Dummy("pouet"))).isTrue();
    }

    @Test
    void shouldGetMatchingEvaluatorForChildClass() {
        AccessEvaluator canEvaluator = new AccessEvaluator(List.of(new ObjectAccessChecker(), new DummyChecker()));

        assertThat(canEvaluator.can(authentication, "action", new DummyChild("pouet"))).isTrue();
        assertThat(canEvaluator.can(authentication, "action", new DummyChild("pouet"))).isTrue();
        logs.assertLogged(Level.INFO, "evaluator", 1);
    }
}
