package com.craft.vdd.kipe;

import static org.assertj.core.api.Assertions.*;

import ch.qos.logback.classic.Level;
import com.craft.vdd.LogSpy;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(LogSpy.class)
class ObjectCheckerUnitTest {

    private static final AccessChecker<Object> checker = new ObjectAccessChecker();

    private final LogSpy logs;

    public ObjectCheckerUnitTest(LogSpy logs) {
        this.logs = logs;
    }

    @Test
    void shouldHandleNullItem() {
        assertThat(checker.can(null, "action", null)).isFalse();

        logs.assertLogged(Level.ERROR, "action");
        logs.assertLogged(Level.ERROR, "unknown");
    }

    @Test
    void shouldHandleEvaluation() {
        assertThat(checker.can(null, "action", "pouet")).isFalse();

        logs.assertLogged(Level.ERROR, "action");
        logs.assertLogged(Level.ERROR, "String");
    }
}
