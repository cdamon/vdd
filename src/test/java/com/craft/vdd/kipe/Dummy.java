package com.craft.vdd.kipe;

class Dummy {

    private final String value;

    public Dummy(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
