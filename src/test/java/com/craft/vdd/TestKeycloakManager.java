package com.craft.vdd;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JsonProvider;
import dasniko.testcontainers.keycloak.KeycloakContainer;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.wait.strategy.Wait;

public class TestKeycloakManager implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {

    private static final RestTemplate rest = new RestTemplate();
    private static JsonProvider jsonReader = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS).jsonProvider();

    private static KeycloakContainer keycloak;
    private static GenericContainer<?> mailhog;
    private static String mailhogUrl;

    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
        if (keycloak != null) {
            return;
        }

        Network network = Network.newNetwork();

        startMailhog(network);
        startKeycloak(network);

        System.setProperty("KEYCLOAK_URL", keycloak.getAuthServerUrl());
        Runtime.getRuntime().addShutdownHook(new Thread(stopContainers()));
    }

    @SuppressWarnings("resource")
    private void startMailhog(Network network) {
        mailhog =
            new GenericContainer<>("mailhog/mailhog")
                .withExposedPorts(8025)
                .withNetwork(network)
                .withNetworkAliases("mailhog")
                .waitingFor(Wait.forHttp("/"));

        mailhog.start();

        mailhogUrl = "http://" + mailhog.getContainerIpAddress() + ":" + mailhog.getMappedPort(8025);
    }

    @SuppressWarnings("resource")
    private void startKeycloak(Network network) {
        keycloak =
            new KeycloakContainer("jboss/keycloak:16.1.0")
                .withRealmImportFiles("/realm-config/vdd-realm.json")
                .withNetwork(network)
                .dependsOn(mailhog);

        keycloak.start();
    }

    private Runnable stopContainers() {
        return () -> {
            keycloak.stop();
            mailhog.stop();
        };
    }

    public static int countEmails() {
        ResponseEntity<String> response = rest.getForEntity(mailhogUrl + "/api/v2/messages", String.class);

        return JsonPath.read(jsonReader.parse(response.getBody()), "$.total");
    }

    public static void wipeEmails() {
        rest.delete(mailhogUrl + "/api/v1/messages");
    }
}
