package com.craft.vdd;

import static com.craft.vdd.cucumber.CucumberAssertions.*;

import io.cucumber.java.en.Then;
import org.springframework.http.HttpStatus;

public class HttpSteps {

    @Then("I should not be authorized")
    public void shouldNotBeAuthorized() {
        assertThatLastResponse().hasHttpStatus(HttpStatus.UNAUTHORIZED);
    }

    @Then("I should be forbidden")
    public void shouldBeForbidden() {
        assertThatLastResponse().hasHttpStatus(HttpStatus.FORBIDDEN);
    }

    @Then("I should have made a bad request")
    public void shouldMakeBadRequest() {
        assertThatLastResponse().hasHttpStatus(HttpStatus.BAD_REQUEST);
    }
}
