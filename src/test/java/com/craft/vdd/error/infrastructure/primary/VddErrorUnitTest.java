package com.craft.vdd.error.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.JsonHelper;
import java.util.List;
import org.junit.jupiter.api.Test;

class VddErrorUnitTest {

    @Test
    void shouldGetErrorInformation() {
        VddFieldError fieldError = VddFieldErrorUnitTest.defaultFieldError();
        VddError error = defaultError(fieldError);

        assertThat(error.getErrorType()).isEqualTo("type");
        assertThat(error.getMessage()).isEqualTo("message");
        assertThat(error.getFieldsErrors()).containsExactly(fieldError);
    }

    @Test
    void shouldSerializeToJson() {
        assertThat(JsonHelper.writeAsString(defaultError(VddFieldErrorUnitTest.defaultFieldError()))).isEqualTo(defaultJson());
    }

    @Test
    void shouldDeserializeFromJson() {
        assertThat(JsonHelper.readFromJson(defaultJson(), VddError.class))
            .usingRecursiveComparison()
            .isEqualTo(defaultError(VddFieldErrorUnitTest.defaultFieldError()));
    }

    private String defaultJson() {
        return "{\"errorType\":\"type\",\"message\":\"message\",\"fieldsErrors\":[" + VddFieldErrorUnitTest.defaultJson() + "]}";
    }

    private VddError defaultError(VddFieldError fieldError) {
        return new VddError("type", "message", List.of(fieldError));
    }
}
