package com.craft.vdd.error.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.domain.ErrorKey;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

class ErrorMessagesUnitTest {

    private static final String ROOT_PACKAGE = "com.craft.vdd";

    private static final Set<Class<? extends ErrorKey>> errors = new Reflections(
        new ConfigurationBuilder()
            .setUrls(ClasspathHelper.forPackage(ROOT_PACKAGE))
            .setScanners(Scanners.SubTypes)
            .filterInputsBy(new FilterBuilder().includePackage(ROOT_PACKAGE))
    )
        .getSubTypesOf(ErrorKey.class);

    @Test
    void shouldHaveOnlyEnumImplementations() {
        errors.forEach(error ->
            assertThat(error.isEnum() || error.isInterface())
                .as("Implementations of " + ErrorKey.class.getName() + " must be enums and " + error.getName() + " wasn't")
                .isTrue()
        );
    }

    @Test
    void shouldNotHaveKeyWithoutKey() {
        assertThat(errors.stream().flatMap(error -> Arrays.stream(error.getEnumConstants()))).noneMatch(errorKey -> errorKey.key() == null);
    }

    @Test
    void shouldHaveMessagesForAllKeys() {
        Collection<Properties> messages = loadMessages();

        errors
            .stream()
            .filter(error -> error.isEnum())
            .forEach(error -> Arrays.stream(error.getEnumConstants()).forEach(value -> messages.forEach(assertMessageExist(value))));
    }

    private List<Properties> loadMessages() {
        try {
            return Files.list(Paths.get("src/main/resources/i18n")).map(toProperties()).collect(Collectors.toList());
        } catch (IOException e) {
            throw new AssertionError();
        }
    }

    private Function<Path, Properties> toProperties() {
        return file -> {
            Properties properties = new Properties();
            try (InputStream input = Files.newInputStream(file)) {
                properties.load(input);
            } catch (IOException e) {
                throw new AssertionError();
            }

            return properties;
        };
    }

    private Consumer<Properties> assertMessageExist(ErrorKey value) {
        return currentMessages -> {
            assertThat(currentMessages.getProperty("error." + value.key()))
                .as("Can't find error message for " + value.key() + " in all files, check your configuration")
                .isNotBlank();
        };
    }
}
