package com.craft.vdd.error.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.domain.ErrorStatus;
import com.craft.vdd.error.domain.MissingMandatoryValueException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

class HttpErrorStatusUnitTest {

    @Test
    void shouldNotConvertFromNullStatus() {
        assertThatThrownBy(() -> HttpErrorStatus.from(null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("status");
    }

    @Test
    void shouldMapAllErrorStatus() {
        Set<HttpErrorStatus> statuses = Arrays.stream(ErrorStatus.values()).map(HttpErrorStatus::from).collect(Collectors.toSet());

        assertThat(statuses).hasSize(ErrorStatus.values().length);
    }
}
