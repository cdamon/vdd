package com.craft.vdd.error.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.craft.vdd.JsonHelper;
import com.craft.vdd.VddIntTest;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Locale;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@VddIntTest
class VddErrorHandlerIntTest {

    private static final Charset UTF_8 = Charset.forName("UTF-8");
    private static final String FRANCE_TAG = Locale.FRANCE.toLanguageTag();

    @Autowired
    private VddErrorHandler exceptionTranslator;

    @Autowired
    private ErrorsResource resource;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(resource).setControllerAdvice(exceptionTranslator).build();
    }

    @Test
    void shouldMapVddExceptions() throws Exception {
        String response = mockMvc
            .perform(post(errorEndpoint("vdd-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG))
            .andExpect(status().isInternalServerError())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response).contains("Une erreur est survenue, notre équipe travaille à sa résolution !");
    }

    @Test
    void shouldMapResponseStatusWithMessageExceptions() throws UnsupportedEncodingException, Exception {
        String response = mockMvc
            .perform(post(errorEndpoint("responsestatus-with-message-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG))
            .andExpect(status().isNotFound())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response).contains("oops");
    }

    @Test
    void shouldMapResponseStatusWithoutMessageExceptions() throws UnsupportedEncodingException, Exception {
        String response = mockMvc
            .perform(post(errorEndpoint("responsestatus-without-message-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG))
            .andExpect(status().isNotFound())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response).contains("est survenue").contains("404");
    }

    @Test
    void shouldGetMessagesInOtherLanguage() throws Exception {
        String response = mockMvc
            .perform(post(errorEndpoint("vdd-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, Locale.UK.toLanguageTag()))
            .andExpect(status().isInternalServerError())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response).contains("An error occurred, our team is working to fix it!");
    }

    @Test
    void shouldGetMessagesInDefaultLanguage() throws Exception {
        String response = mockMvc
            .perform(post(errorEndpoint("vdd-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, Locale.CHINESE.toLanguageTag()))
            .andExpect(status().isInternalServerError())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response).contains("Une erreur est survenue, notre équipe travaille à sa résolution !");
    }

    @Test
    void shouldMapParametersBeanValidationErrors() throws Exception {
        String response = mockMvc
            .perform(get(errorEndpoint("oops")).header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG))
            .andExpect(status().isBadRequest())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response)
            .contains("Les données que vous avez saisies ne sont pas valides.")
            .contains("Le format n'est pas correct, il doit respecter \\\"complicated\\\".");
    }

    @Test
    void shouldMapMinValueQueryStringBeanValidationErrors() throws Exception {
        String response = mockMvc
            .perform(get(errorEndpoint("?parameter=1")).header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG))
            .andExpect(status().isBadRequest())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response)
            .contains("Les données que vous avez saisies ne sont pas valides.")
            .contains("La valeur que vous avez entrée est trop petite, le minimum autorisé est 42.");
    }

    @Test
    void shouldMapMaxValueQueryStringBeanValidationErrors() throws Exception {
        String response = mockMvc
            .perform(get(errorEndpoint("?parameter=100")).header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG))
            .andExpect(status().isBadRequest())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response)
            .contains("Les données que vous avez saisies ne sont pas valides.")
            .contains("La valeur que vous avez entrée est trop grande, le maximum autorisé est 42.");
    }

    @Test
    void shouldMapBodyBeanValidationErrors() throws Exception {
        String response = mockMvc
            .perform(
                post(errorEndpoint("oops"))
                    .header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(JsonHelper.writeAsString(new ComplicatedRequest("value")))
            )
            .andExpect(status().isBadRequest())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response)
            .contains("Les données que vous avez saisies ne sont pas valides.")
            .contains("Le format n'est pas correct, il doit respecter \\\"complicated\\\".");
    }

    @Test
    void shouldMapTechnicalError() throws Exception {
        String response = mockMvc
            .perform(
                post(errorEndpoint("not-deserializables"))
                    .header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(JsonHelper.writeAsString(new NotDeserializable("value")))
            )
            .andExpect(status().isInternalServerError())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response).contains("Une erreur est survenue, notre équipe travaille à sa résolution !");
    }

    @Test
    void shouldHandleAccessDeniedException() throws Exception {
        String response = mockMvc
            .perform(get(errorEndpoint("access-denied")).header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG))
            .andExpect(status().isForbidden())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response).contains("Vous n'avez pas les droits suffisants");
    }

    @Test
    void shouldHandleRuntimeException() throws Exception {
        String response = mockMvc
            .perform(get(errorEndpoint("runtime-exceptions")).header(HttpHeaders.ACCEPT_LANGUAGE, FRANCE_TAG))
            .andExpect(status().isInternalServerError())
            .andReturn()
            .getResponse()
            .getContentAsString(UTF_8);

        assertThat(response).contains("Une erreur est survenue, notre équipe travaille à sa résolution !");
    }

    private URI errorEndpoint(String path) {
        try {
            return new URI("/errors/" + path);
        } catch (URISyntaxException e) {
            throw new AssertionError();
        }
    }
}
