package com.craft.vdd.error.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import ch.qos.logback.classic.Level;
import com.craft.vdd.LogSpy;
import com.craft.vdd.error.domain.AssertErrorKey;
import com.craft.vdd.error.domain.ErrorStatus;
import com.craft.vdd.error.domain.StandardErrorKey;
import com.craft.vdd.error.domain.VddException;
import com.craft.vdd.message.domain.Messages;
import java.nio.charset.Charset;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@ExtendWith({ SpringExtension.class, LogSpy.class })
class VddErrorHandlerUnitTest {

    @Mock
    private Messages messages;

    @InjectMocks
    private VddErrorHandler handler;

    private final LogSpy logs;

    public VddErrorHandlerUnitTest(LogSpy logs) {
        this.logs = logs;
    }

    @Test
    void shouldHandleVddException() {
        RuntimeException cause = new RuntimeException();
        VddException exception = VddException
            .builder(AssertErrorKey.MISSING_MANDATORY_VALUE)
            .cause(cause)
            .status(ErrorStatus.BAD_REQUEST)
            .message("Hum")
            .build();

        when(messages.get(eq("error.missing-mandatory-value"), any())).thenReturn("User message");
        ResponseEntity<VddError> response = handler.handleVddException(exception);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        VddError body = response.getBody();
        assertThat(body.getErrorType()).isEqualTo("missing-mandatory-value");
        assertThat(body.getMessage()).isEqualTo("User message");
        assertThat(body.getFieldsErrors()).isNull();
    }

    @Test
    void shouldHandleExceededSizeFileUpload() {
        when(messages.get(eq("error.server.upload-too-big"), any())).thenReturn("The file is too big to be send to the server");

        ResponseEntity<VddError> response = handler.handleFileSizeException(new MaxUploadSizeExceededException(1024 * 1024 * 30));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getErrorType()).isEqualTo("server.upload-too-big");
        assertThat(response.getBody().getMessage()).isEqualTo("The file is too big to be send to the server");
    }

    @Test
    void shouldHandleMethodArgumentTypeMismatchException() {
        MethodArgumentTypeMismatchException mismatchException = new MethodArgumentTypeMismatchException(null, null, null, null, null);

        ResponseEntity<VddError> response = handler.handleMethodArgumentTypeMismatchException(mismatchException);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getErrorType()).isEqualTo("user.bad-request");
    }

    @Test
    void shouldHandleMethodArgumentTypeMismatchExceptionWithCause() {
        RuntimeException cause = VddException.builder(AssertErrorKey.MISSING_MANDATORY_VALUE).build();
        MethodArgumentTypeMismatchException mismatchException = new MethodArgumentTypeMismatchException(null, null, null, null, cause);

        ResponseEntity<VddError> response = handler.handleMethodArgumentTypeMismatchException(mismatchException);

        assertThat(response.getBody().getErrorType()).isEqualTo("missing-mandatory-value");
    }

    @Test
    void shouldLogUserErrorAsWarn() {
        handler.handleVddException(
            VddException.builder(AssertErrorKey.MISSING_MANDATORY_VALUE).status(ErrorStatus.BAD_REQUEST).message("error message").build()
        );

        logs.assertLogged(Level.WARN, "error message");
    }

    @Test
    void shouldLogAuthenticationExceptionAsDebug() {
        handler.handleAuthenticationException(new InsufficientAuthenticationException("oops"));

        logs.assertLogged(Level.DEBUG, "oops");
    }

    @Test
    void shouldLogServerErrorAsError() {
        handler.handleVddException(
            VddException
                .builder(StandardErrorKey.TECHNICAL_ERROR)
                .status(ErrorStatus.INTERNAL_SERVER_ERROR)
                .message("error message")
                .build()
        );

        logs.assertLogged(Level.ERROR, "error message");
    }

    @Test
    void shouldLogErrorResponseBody() {
        RestClientResponseException cause = new RestClientResponseException(
            "error",
            400,
            "status",
            null,
            "service error response".getBytes(),
            Charset.defaultCharset()
        );

        handler.handleVddException(
            VddException
                .builder(StandardErrorKey.TECHNICAL_ERROR)
                .status(ErrorStatus.INTERNAL_SERVER_ERROR)
                .message("error message")
                .cause(cause)
                .build()
        );

        logs.assertLogged(Level.ERROR, "error message");
        logs.assertLogged(Level.ERROR, "service error response");
    }
}
