package com.craft.vdd.error.infrastructure.primary;

import com.craft.vdd.common.infrastructure.primary.ValidationMessage;
import com.craft.vdd.error.domain.ErrorStatus;
import com.craft.vdd.error.domain.StandardErrorKey;
import com.craft.vdd.error.domain.VddException;
import javax.validation.constraints.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@Validated
@RestController
@RequestMapping("/errors")
class ErrorsResource {

    private static final Logger logger = LoggerFactory.getLogger(ErrorsResource.class);

    @GetMapping("/runtime-exceptions")
    public void runtimeException() {
        throw new RuntimeException();
    }

    @GetMapping("/access-denied")
    public void accessDeniedException() {
        throw new AccessDeniedException("You shall not pass!");
    }

    @PostMapping("/vdd-exceptions")
    public void vddException() {
        throw VddException
            .builder(StandardErrorKey.TECHNICAL_ERROR)
            .cause(new RuntimeException())
            .status(ErrorStatus.INTERNAL_SERVER_ERROR)
            .message("Oops")
            .argument("key", "value")
            .build();
    }

    @PostMapping("/responsestatus-with-message-exceptions")
    public void responseStatusWithMessageException() {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "oops");
    }

    @PostMapping("/responsestatus-without-message-exceptions")
    public void responseStatusWithoutMessageException() {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @GetMapping
    public void queryStringWithRangedValue(@Validated QueryParameter parameter) {}

    @GetMapping("/{complicated}")
    public void complicatedArg(
        @Validated @Pattern(message = ValidationMessage.WRONG_FORMAT, regexp = "complicated") @PathVariable(
            "complicated"
        ) String complicated
    ) {
        logger.info("Congratulations you got it right!");
    }

    @PostMapping("/oops")
    public void complicatedBody(@Validated @RequestBody ComplicatedRequest request) {
        logger.info("You got it right!");
    }

    @PostMapping("/not-deserializables")
    public void notDeserializable(@RequestBody @Validated NotDeserializable request) {}
}
