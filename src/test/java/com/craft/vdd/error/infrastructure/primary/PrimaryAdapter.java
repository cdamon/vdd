package com.craft.vdd.error.infrastructure.primary;

import com.craft.vdd.error.domain.StandardErrorKey;
import com.craft.vdd.error.domain.VddException;

public class PrimaryAdapter {

    public static void fail() {
        throw VddException.builder(StandardErrorKey.TECHNICAL_ERROR).build();
    }
}
