package com.craft.vdd.error.infrastructure.primary;

import com.craft.vdd.common.infrastructure.primary.ValidationMessage;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

class QueryParameter {

    private int parameter;

    @Min(message = ValidationMessage.VALUE_TOO_LOW, value = 42)
    @Max(message = ValidationMessage.VALUE_TOO_HIGH, value = 42)
    public int getParameter() {
        return parameter;
    }

    public void setParameter(int parameter) {
        this.parameter = parameter;
    }
}
