package com.craft.vdd.error.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.JsonHelper;
import org.junit.jupiter.api.Test;

class VddFieldErrorUnitTest {

    @Test
    void shouldGetFieldErrorInformation() {
        VddFieldError fieldError = defaultFieldError();

        assertThat(fieldError.getFieldPath()).isEqualTo("path");
        assertThat(fieldError.getReason()).isEqualTo("reason");
        assertThat(fieldError.getMessage()).isEqualTo("message");
    }

    @Test
    void shouldSerializeToJson() {
        assertThat(JsonHelper.writeAsString(defaultFieldError())).isEqualTo(defaultJson());
    }

    static VddFieldError defaultFieldError() {
        return VddFieldError.builder().fieldPath("path").reason("reason").message("message").build();
    }

    static String defaultJson() {
        return "{\"fieldPath\":\"path\",\"reason\":\"reason\",\"message\":\"message\"}";
    }
}
