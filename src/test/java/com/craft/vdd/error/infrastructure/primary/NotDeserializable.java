package com.craft.vdd.error.infrastructure.primary;

public class NotDeserializable {

    private String value;

    public NotDeserializable(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
