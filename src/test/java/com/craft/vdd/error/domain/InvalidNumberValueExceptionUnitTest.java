package com.craft.vdd.error.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InvalidNumberValueExceptionUnitTest {

    @Test
    void shouldGetUnderMinExceptionInformation() {
        InvalidNumberValueException exception = InvalidNumberValueException.underMin("fieldName").value("42").min(1337).build();

        assertThat(exception.key()).isEqualTo(AssertErrorKey.VALUE_UNDER_MIN);
        assertThat(exception.message()).contains("fieldName").contains("42").contains("1337");
        assertThat(exception.arguments()).containsOnly(entry("field", "fieldName"), entry("value", "42"), entry("min", (double) 1337));
    }

    @Test
    void shouldGetOverMaxExceptionInformation() {
        InvalidNumberValueException exception = InvalidNumberValueException.overMax("fieldName").value(1337).max(42).build();

        assertThat(exception.key()).isEqualTo(AssertErrorKey.VALUE_OVER_MAX);
        assertThat(exception.message()).contains("fieldName").contains("42").contains("1337");
        assertThat(exception.arguments())
            .containsOnly(entry("field", "fieldName"), entry("value", (double) 1337), entry("max", (double) 42));
    }
}
