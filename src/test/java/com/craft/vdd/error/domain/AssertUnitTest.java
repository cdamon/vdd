package com.craft.vdd.error.domain;

import static org.assertj.core.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;

class AssertUnitTest {

    @Test
    void shouldNotValidateNullObjectAsNotNull() {
        assertThatThrownBy(() -> Assert.notNull("fieldName", null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("fieldName");
    }

    @Test
    void shouldValidateNotNullObjectAsNotNull() {
        assertThatCode(() -> Assert.notNull("field", "toto")).doesNotThrowAnyException();
    }

    @Test
    void shouldNotValidateNullStringAsNotBlank() {
        assertThatThrownBy(() -> Assert.notBlank("fieldName", null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("fieldName");
    }

    @Test
    void shouldNotValidateBlankStringAsNotBlank() {
        assertThatThrownBy(() -> Assert.notBlank("fieldName", " "))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("fieldName");
    }

    @Test
    void shouldValidateActualStringAsNotBlank() {
        assertThatCode(() -> Assert.notBlank("field", "toto")).doesNotThrowAnyException();
    }

    @Test
    void shouldNotValidateNullCollectionAsNotEmpty() {
        assertThatThrownBy(() -> Assert.notEmpty("field", null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("field");
    }

    @Test
    void shouldNotValidateEmptyCollectionAsNotEmpty() {
        assertThatThrownBy(() -> Assert.notEmpty("field", List.of()))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("field");
    }

    @Test
    void shouldValidateActualCollectionAsNotEmpty() {
        assertThatCode(() -> Assert.notEmpty("field", List.of("hey"))).doesNotThrowAnyException();
    }

    @Test
    void shouldNotValidateBlankStringAsNotBlankForFluentAssertions() {
        assertThatThrownBy(() -> Assert.field("fieldName", " ").notBlank())
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("fieldName");
    }

    @Test
    void shouldValidateActualStringAsNotBlankForFluentAssertions() {
        assertThatCode(() -> Assert.field("fieldName", "hey").notBlank()).doesNotThrowAnyException();
    }

    @Test
    void shouldNotValidateStringOverMaxLength() {
        assertThatThrownBy(() -> Assert.field("fieldName", "value").maxLength(4))
            .isExactlyInstanceOf(StringTooLongException.class)
            .hasMessageContaining("fieldName")
            .hasMessageContaining("5")
            .hasMessageContaining("4");
    }

    @Test
    void shouldValidateMaxLengthForNullStringValue() {
        assertThatCode(() -> Assert.field("field", (String) null).maxLength(1)).doesNotThrowAnyException();
    }

    @Test
    void shouldValidateShortEnoughString() {
        assertThatCode(() -> Assert.field("field", "value").maxLength(6)).doesNotThrowAnyException();
    }

    @Test
    void shouldValidateStringAtLength() {
        assertThatCode(() -> Assert.field("field", "value").maxLength(5)).doesNotThrowAnyException();
    }

    @Test
    void shouldNotValidateValueUnderMinValue() {
        assertThatThrownBy(() -> Assert.field("field", 42).min(1337))
            .isExactlyInstanceOf(InvalidNumberValueException.class)
            .hasMessageContaining("field")
            .hasMessageContaining("42")
            .hasMessageContaining("1337");
    }

    @Test
    void shouldValidateValueAtMinValue() {
        assertThatCode(() -> Assert.field("field", 1337).min(1337)).doesNotThrowAnyException();
    }

    @Test
    void shouldValidateValueOverMinValue() {
        assertThatCode(() -> Assert.field("field", 10000).min(1337)).doesNotThrowAnyException();
    }

    @Test
    void shouldNotValidateValueOverMaxValue() {
        assertThatThrownBy(() -> Assert.field("field", 1337).max(42))
            .isExactlyInstanceOf(InvalidNumberValueException.class)
            .hasMessageContaining("field")
            .hasMessageContaining("42")
            .hasMessageContaining("1337");
    }

    @Test
    void shouldValidateValueAtMaxValue() {
        assertThatCode(() -> Assert.field("field", 1337).max(1337)).doesNotThrowAnyException();
    }

    @Test
    void shouldValidateValueUnderMaxValue() {
        assertThatCode(() -> Assert.field("field", 42).max(1337)).doesNotThrowAnyException();
    }

    @Test
    void shouldNotValidateNullBigDecimalAsNotNull() {
        assertThatThrownBy(() -> Assert.field("field", (BigDecimal) null).notNull())
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("field");
    }

    @Test
    void shouldValidateNotNullBigDecimalAsNotNull() {
        assertThatCode(() -> Assert.field("field", BigDecimal.ZERO).notNull()).doesNotThrowAnyException();
    }

    @Test
    void shouldNotValidateNullBigDecimalAsStrictlyPositive() {
        assertThatThrownBy(() -> Assert.field("field", (BigDecimal) null).positive())
            .isExactlyInstanceOf(InvalidNumberValueException.class)
            .hasMessageContaining("field")
            .hasMessageContaining("0")
            .hasMessageContaining("null");
    }

    @Test
    void shouldNotValidateNegativeBigDecimalAsStrictlyPositive() {
        assertThatThrownBy(() -> Assert.field("field", BigDecimal.valueOf(-1)).positive())
            .isExactlyInstanceOf(InvalidNumberValueException.class)
            .hasMessageContaining("field")
            .hasMessageContaining("0")
            .hasMessageContaining("-1");
    }

    @Test
    void shouldNotValidateZeroBigDecimalAsStrictlyPositive() {
        assertThatCode(() -> Assert.field("field", BigDecimal.ZERO).positive()).doesNotThrowAnyException();
    }

    @Test
    void shouldValidatePositiveBigDecimalAsStricltyPositive() {
        assertThatCode(() -> Assert.field("field", BigDecimal.ONE).positive()).doesNotThrowAnyException();
    }
}
