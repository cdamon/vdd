package com.craft.vdd.error.domain;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.infrastructure.primary.PrimaryAdapter;
import org.junit.jupiter.api.Test;

class VddExceptionUnitTest {

    @Test
    void shouldNotBuildWithoutBuilder() {
        assertThatThrownBy(() -> new VddException(null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("builder");
    }

    @Test
    void shouldGetDefaultExceptionInformation() {
        VddException exception = VddException.builder(null).build();

        assertThat(exception.key()).isEqualTo(StandardErrorKey.TECHNICAL_ERROR);
        assertThat(exception.getCause()).isNull();
        assertThat(exception.cause()).isNull();
        assertThat(exception.getMessage()).isEqualTo("A technical error occured");
        assertThat(exception.message()).isEqualTo("A technical error occured");
        assertThat(exception.status()).isEqualTo(ErrorStatus.INTERNAL_SERVER_ERROR);
        assertThat(exception.arguments()).isEmpty();
    }

    @Test
    void shouldGetBadRequestByDefaultForExceptionCommingFromPrimary() {
        try {
            PrimaryAdapter.fail();
            fail("oops");
        } catch (VddException e) {
            assertThat(e.status()).isEqualTo(ErrorStatus.BAD_REQUEST);
        }
    }

    @Test
    void shouldIgnoreArgumentWithoutKey() {
        VddException exception = VddException.builder(null).argument(null, "value").build();

        assertThat(exception.arguments()).isEmpty();
    }

    @Test
    void shouldGetExceptionInformation() {
        RuntimeException cause = new RuntimeException();

        VddException exception = VddException
            .builder(AssertErrorKey.MISSING_MANDATORY_VALUE)
            .message("Oops")
            .cause(cause)
            .status(ErrorStatus.BAD_REQUEST)
            .argument("key", "value")
            .build();

        assertThat(exception.key()).isEqualTo(AssertErrorKey.MISSING_MANDATORY_VALUE);
        assertThat(exception.getCause()).isEqualTo(cause);
        assertThat(exception.cause()).isEqualTo(cause);
        assertThat(exception.getMessage()).isEqualTo("Oops");
        assertThat(exception.message()).isEqualTo("Oops");
        assertThat(exception.status()).isEqualTo(ErrorStatus.BAD_REQUEST);
        assertThat(exception.arguments()).containsExactly(entry("key", "value"));
    }

    @Test
    void shouldGetInternalServerErrorException() {
        VddException exception = VddException.internalServerError(AssertErrorKey.MISSING_MANDATORY_VALUE).build();

        assertThat(exception.status()).isEqualTo(ErrorStatus.INTERNAL_SERVER_ERROR);
        assertThat(exception.key()).isEqualTo(AssertErrorKey.MISSING_MANDATORY_VALUE);
    }

    @Test
    void shouldGetBadRequestException() {
        VddException exception = VddException.badRequest(AssertErrorKey.MISSING_MANDATORY_VALUE).build();

        assertThat(exception.status()).isEqualTo(ErrorStatus.BAD_REQUEST);
        assertThat(exception.key()).isEqualTo(AssertErrorKey.MISSING_MANDATORY_VALUE);
    }

    @Test
    void shouldGetNotFoundException() {
        VddException exception = VddException.notFound(AssertErrorKey.MISSING_MANDATORY_VALUE).build();

        assertThat(exception.status()).isEqualTo(ErrorStatus.NOT_FOUND);
        assertThat(exception.key()).isEqualTo(AssertErrorKey.MISSING_MANDATORY_VALUE);
    }

    @Test
    void shouldGetUnauthorizedException() {
        VddException exception = VddException.unauthorized(AssertErrorKey.MISSING_MANDATORY_VALUE).build();

        assertThat(exception.status()).isEqualTo(ErrorStatus.UNAUTHORIZED);
        assertThat(exception.key()).isEqualTo(AssertErrorKey.MISSING_MANDATORY_VALUE);
    }
}
