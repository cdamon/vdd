package com.craft.vdd.error.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StringTooLongExceptionUnitTest {

    @Test
    void shouldGetExceptionInformation() {
        StringTooLongException exception = StringTooLongException.builder().length(50).maxLength(42).field("catNumber").build();

        assertThat(exception.key()).isEqualTo(AssertErrorKey.STRING_TOO_LONG);
        assertThat(exception.arguments()).containsOnly(entry("field", "catNumber"), entry("length", 50), entry("maxLength", 42));
        assertThat(exception.message()).isEqualTo("Value in catNumber was 50 but max length is 42");
    }
}
