package com.craft.vdd.error.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MissingMandatoryValueExceptionUnitTest {

    @Test
    void shouldGetExceptionInformation() {
        MissingMandatoryValueException exception = new MissingMandatoryValueException("fieldName");

        assertThat(exception.key()).isEqualTo(AssertErrorKey.MISSING_MANDATORY_VALUE);
        assertThat(exception.arguments()).containsExactly(entry("field", "fieldName"));
        assertThat(exception.message()).contains("mandatory").contains("fieldName");
    }
}
