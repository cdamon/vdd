package com.craft.vdd.message.infrastructure.secondary;

import static org.assertj.core.api.Assertions.*;

import ch.qos.logback.classic.Level;
import com.craft.vdd.LogSpy;
import com.craft.vdd.error.domain.MissingMandatoryValueException;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.support.ResourceBundleMessageSource;

@ExtendWith(LogSpy.class)
class MessageSourceMessagesUnitTest {

    private static final MessageSourceMessages messages = buildMessages();
    private final LogSpy logs;

    public MessageSourceMessagesUnitTest(LogSpy logs) {
        this.logs = logs;
    }

    private static MessageSourceMessages buildMessages() {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();

        source.setBasename("i18n/messages");
        source.setDefaultEncoding("UTF-8");

        return new MessageSourceMessages(source);
    }

    @Test
    void shouldNotGetMessageWithoutKey() {
        assertThatThrownBy(() -> messages.get(null)).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("key");
    }

    @Test
    void shouldNotGetMessageWithBlankKey() {
        assertThatThrownBy(() -> messages.get(" ")).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("key");
    }

    @Test
    void shouldGetKeyForUnknownMessage() {
        assertThat(messages.get("unknown")).isEqualTo("unknown");

        logs.assertLogged(Level.ERROR, "unknown");
    }

    @Test
    void shouldGetMessageWithReplacedParameters() {
        assertThat(messages.get("error.missing-mandatory-value", Map.of("field", "fieldName")))
            .isEqualTo("Le champ fieldName doit être renseigné.");
    }
}
