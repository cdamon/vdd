package com.craft.vdd;

import com.craft.vdd.authentication.domain.Authorities;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

public final class TestAuthentications {

    private TestAuthentications() {}

    public static Authentication admin() {
        return new TestingAuthenticationToken("admin", null, Authorities.ADMIN);
    }

    public static Authentication user() {
        return user(Authorities.USER);
    }

    public static TestingAuthenticationToken user(String role) {
        return new TestingAuthenticationToken("user", null, role);
    }
}
