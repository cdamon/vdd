package com.craft.vdd.cucumber;

import org.springframework.http.HttpStatus;

public interface ResponseAsserter<T extends ResponseAsserter<?>> {
    T hasHttpStatus(HttpStatus status);

    ElementAsserter<?> hasElement(String jsonPath);

    HeaderAsserter<?> hasHeader(String header);

    default ElementAsserter<?> hasResponse() {
        return hasElement(null);
    }

    default T hasOkStatus() {
        return hasHttpStatus(200);
    }

    default T hasHttpStatus(int status) {
        return hasHttpStatus(HttpStatus.valueOf(status));
    }

    public T hasRawBody(String info);
}
