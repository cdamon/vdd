package com.craft.vdd.cucumber;

import static org.assertj.core.api.Assertions.*;

import java.util.List;
import java.util.stream.Collectors;

public class HeaderAsserter<T extends ResponseAsserter<?>> {

    private final T responseAsserter;
    private final String header;

    HeaderAsserter(T responseAsserter, String header) {
        this.responseAsserter = responseAsserter;
        this.header = header;
    }

    public T and() {
        return responseAsserter;
    }

    public HeaderAsserter<T> contaning(String value) {
        List<String> values = CucumberTestContext.getResponseHeader(header);
        assertThat(values)
            .as("Expecting header " + header + " to contain " + value + " but can't find it, current values are " + displayValues(values))
            .contains(value);

        return this;
    }

    public HeaderAsserter<T> startingWith(String prefix) {
        List<String> values = CucumberTestContext.getResponseHeader(header);
        assertThat(values)
            .as(
                "Expecting header " +
                header +
                " to start with " +
                prefix +
                " but can't find it, current values are " +
                displayValues(values)
            )
            .anyMatch(content -> content.startsWith(prefix));

        return this;
    }

    private String displayValues(List<String> values) {
        return values.stream().collect(Collectors.joining(", "));
    }
}
