package com.craft.vdd.pagination.infrastructure.primary;

import static com.craft.vdd.pagination.domain.VddPagesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.domain.MissingMandatoryValueException;
import java.util.List;
import java.util.function.Function;
import org.junit.jupiter.api.Test;

class RestVddPageUnitTest {

    @Test
    void shouldNotConvertWithoutSourcePage() {
        assertThatThrownBy(() -> RestVddPage.from(null, source -> "test")).isExactlyInstanceOf(MissingMandatoryValueException.class);
    }

    @Test
    void shouldNotConvertWithoutMappingFunction() {
        assertThatThrownBy(() -> RestVddPage.from(page(), null)).isExactlyInstanceOf(MissingMandatoryValueException.class);
    }

    @Test
    void shouldMapFromDomainPage() {
        RestVddPage<String> page = RestVddPage.from(page(), Function.identity());

        assertThat(page.getContent()).containsExactly("test");
        assertThat(page.getCurrentPage()).isEqualTo(2);
        assertThat(page.getPageSize()).isEqualTo(10);
        assertThat(page.getTotalElementsCount()).isEqualTo(21);
        assertThat(page.getPagesCount()).isEqualTo(3);
    }

    @Test
    void shouldGetPageCountForPageLimit() {
        RestVddPage<String> page = RestVddPage.from(pageBuilder().totalElementsCount(3).pageSize(3).build(), Function.identity());

        assertThat(page.getPagesCount()).isEqualTo(1);
    }

    @Test
    void shouldNotConvertFromListWithoutList() {
        assertThatThrownBy(() -> RestVddPage.from(null, new RestVddPageable(), Function.identity()))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("source");
    }

    @Test
    void shouldNotConvertFromListWithoutPagination() {
        assertThatThrownBy(() -> RestVddPage.from(List.of(), null, Function.identity()))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("pagination");
    }

    @Test
    void shouldNotConvertFromListWithoutMapper() {
        assertThatThrownBy(() -> RestVddPage.from(List.of(), new RestVddPageable(), null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("mapper");
    }

    @Test
    void shouldConvertFromList() {
        RestVddPageable pagination = new RestVddPageable();
        pagination.setPageSize(2);

        RestVddPage<String> page = RestVddPage.from(List.of("titi", "toto", "tata"), pagination, String::toUpperCase);

        assertThat(page.getContent()).containsExactly("TITI", "TOTO");
        assertThat(page.getCurrentPage()).isZero();
        assertThat(page.getPageSize()).isEqualTo(2);
        assertThat(page.getTotalElementsCount()).isEqualTo(3);
        assertThat(page.getPagesCount()).isEqualTo(2);
    }

    @Test
    void shouldConvertFromSmallList() {
        RestVddPageable pagination = new RestVddPageable();
        pagination.setPageSize(2);

        RestVddPage<String> page = RestVddPage.from(List.of("titi"), pagination, String::toUpperCase);

        assertThat(page.getContent()).containsExactly("TITI");
        assertThat(page.getCurrentPage()).isZero();
        assertThat(page.getPageSize()).isEqualTo(2);
        assertThat(page.getTotalElementsCount()).isEqualTo(1);
        assertThat(page.getPagesCount()).isEqualTo(1);
    }
}
