package com.craft.vdd.pagination.infrastructure.primary;

import static com.craft.vdd.BeanValidationAssertions.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.common.infrastructure.primary.ValidationMessage;
import com.craft.vdd.pagination.domain.VddPageable;
import org.junit.jupiter.api.Test;

class RestVddPageableUnitTest {

    @Test
    void shouldConvertToDomain() {
        VddPageable pageable = pageable().toPageable();

        assertThat(pageable.page()).isEqualTo(1);
        assertThat(pageable.pageSize()).isEqualTo(15);
    }

    @Test
    void shouldNotValidateWithPageUnderZero() {
        RestVddPageable pageable = pageable();
        pageable.setPage(-1);

        assertThatBean(pageable).hasInvalidProperty("page").withMessage(ValidationMessage.VALUE_TOO_LOW);
    }

    @Test
    void shouldNotValidateWithSizeAtZero() {
        RestVddPageable pageable = pageable();
        pageable.setPageSize(0);

        assertThatBean(pageable).hasInvalidProperty("pageSize").withMessage(ValidationMessage.VALUE_TOO_LOW).withParameter("value", 1L);
    }

    @Test
    void shouldNotValidateWithPageSizeOverHundred() {
        RestVddPageable pageable = pageable();
        pageable.setPageSize(101);

        assertThatBean(pageable).hasInvalidProperty("pageSize").withMessage(ValidationMessage.VALUE_TOO_HIGH);
    }

    private RestVddPageable pageable() {
        return new RestVddPageable(1, 15);
    }
}
