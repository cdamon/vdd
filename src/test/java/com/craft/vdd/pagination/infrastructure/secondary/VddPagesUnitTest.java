package com.craft.vdd.pagination.infrastructure.secondary;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.domain.MissingMandatoryValueException;
import com.craft.vdd.pagination.domain.VddPage;
import com.craft.vdd.pagination.domain.VddPageable;
import java.util.List;
import java.util.function.Function;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class VddPagesUnitTest {

    @Test
    void shouldNotBuildPageableFromNullPVddPageable() {
        assertThatThrownBy(() -> VddPages.from(null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("pagination");
    }

    @Test
    void shouldBuildPageableFromVddPageable() {
        Pageable pagination = VddPages.from(pagination());

        assertThat(pagination.getPageNumber()).isEqualTo(2);
        assertThat(pagination.getPageSize()).isEqualTo(15);
        assertThat(pagination.getSort()).isEqualTo(Sort.unsorted());
    }

    @Test
    void shouldNotBuildWithoutSort() {
        assertThatThrownBy(() -> VddPages.from(pagination(), null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("sort");
    }

    @Test
    void shouldBuildPageableFromVddPageableAndSort() {
        Pageable pagination = VddPages.from(pagination(), Sort.by("dummy"));

        assertThat(pagination.getPageNumber()).isEqualTo(2);
        assertThat(pagination.getPageSize()).isEqualTo(15);
        assertThat(pagination.getSort()).isEqualTo(Sort.by("dummy"));
    }

    private VddPageable pagination() {
        return new VddPageable(2, 15);
    }

    @Test
    void shouldNotConvertFromSpringPageWithoutSpringPage() {
        assertThatThrownBy(() -> VddPages.from(null, source -> source))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("springPage");
    }

    @Test
    void shouldNotConvertFromSpringPageWithoutMapper() {
        assertThatThrownBy(() -> VddPages.from(springPage(), null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("mapper");
    }

    @Test
    void shouldConvertFromSpringPage() {
        VddPage<String> page = VddPages.from(springPage(), Function.identity());

        assertThat(page.content()).containsExactly("test");
        assertThat(page.currentPage()).isEqualTo(2);
        assertThat(page.pageSize()).isEqualTo(10);
        assertThat(page.totalElementsCount()).isEqualTo(30);
    }

    private PageImpl<String> springPage() {
        return new PageImpl<>(List.of("test"), PageRequest.of(2, 10), 30);
    }
}
