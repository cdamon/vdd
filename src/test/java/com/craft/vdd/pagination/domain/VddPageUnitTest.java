package com.craft.vdd.pagination.domain;

import static com.craft.vdd.pagination.domain.VddPagesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.domain.MissingMandatoryValueException;
import java.util.List;
import org.junit.jupiter.api.Test;

class VddPageUnitTest {

    @Test
    void shouldGetEmptySinglePageWithoutContent() {
        VddPage<String> page = VddPage.singlePage(null);

        assertEmptyPage(page);
    }

    @Test
    void shouldGetEmptySinglePageFromBuilderWithoutContent() {
        VddPage<?> page = VddPage.builder(null).build();

        assertEmptyPage(page);
    }

    private void assertEmptyPage(VddPage<?> page) {
        assertThat(page.content()).isEmpty();
        assertThat(page.currentPage()).isZero();
        assertThat(page.pageSize()).isZero();
        assertThat(page.totalElementsCount()).isZero();
    }

    @Test
    void shouldGetSinglePage() {
        VddPage<String> page = VddPage.singlePage(List.of("test", "dummy"));

        assertSinglePage(page);
    }

    @Test
    void shouldGetSinglePageFromBuilderWithContentOnly() {
        VddPage<String> page = VddPage.builder(List.of("test", "dummy")).build();

        assertSinglePage(page);
    }

    private void assertSinglePage(VddPage<String> page) {
        assertThat(page.content()).containsExactly("test", "dummy");
        assertThat(page.currentPage()).isZero();
        assertThat(page.pageSize()).isEqualTo(2);
        assertThat(page.totalElementsCount()).isEqualTo(2);
        assertThat(page.pageCount()).isEqualTo(1);
    }

    @Test
    void shouldGetFullPage() {
        VddPage<String> page = pageBuilder().build();

        assertThat(page.content()).containsExactly("test");
        assertThat(page.currentPage()).isEqualTo(2);
        assertThat(page.pageSize()).isEqualTo(10);
        assertThat(page.totalElementsCount()).isEqualTo(21);
        assertThat(page.pageCount()).isEqualTo(3);
    }

    @Test
    void shouldNotMapWithoutMapper() {
        assertThatThrownBy(() -> pageBuilder().build().map(null))
            .isExactlyInstanceOf(MissingMandatoryValueException.class)
            .hasMessageContaining("mapper");
    }

    @Test
    void shouldMapPage() {
        VddPage<String> page = pageBuilder().build().map(entry -> "hey");

        assertThat(page.content()).containsExactly("hey");
        assertThat(page.currentPage()).isEqualTo(2);
        assertThat(page.pageSize()).isEqualTo(10);
        assertThat(page.totalElementsCount()).isEqualTo(21);
        assertThat(page.pageCount()).isEqualTo(3);
    }

    @Test
    void shouldNotBeLastForFirstPage() {
        assertThat(pageBuilder().currentPage(0).build().isNotLast()).isTrue();
    }

    @Test
    void shouldBeLastWithOnePage() {
        assertThat(VddPage.singlePage(List.of("d")).isNotLast()).isFalse();
    }

    @Test
    void shouldBeLastPageWithoutContent() {
        VddPage<Object> page = VddPage.builder(List.of()).currentPage(0).pageSize(1).totalElementsCount(0).build();
        assertThat(page.isNotLast()).isFalse();
    }

    @Test
    void shouldBeLastForLastPage() {
        assertThat(pageBuilder().currentPage(2).build().isNotLast()).isFalse();
    }
}
