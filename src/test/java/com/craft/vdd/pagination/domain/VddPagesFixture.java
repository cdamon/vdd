package com.craft.vdd.pagination.domain;

import com.craft.vdd.pagination.domain.VddPage.VddPageBuilder;
import java.util.List;

public final class VddPagesFixture {

    private VddPagesFixture() {}

    public static VddPage<String> page() {
        return pageBuilder().build();
    }

    public static VddPageBuilder<String> pageBuilder() {
        return VddPage.builder(List.of("test")).currentPage(2).pageSize(10).totalElementsCount(21);
    }
}
