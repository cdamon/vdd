package com.craft.vdd.pagination.domain;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.domain.InvalidNumberValueException;
import org.junit.jupiter.api.Test;

class VddPageableUnitTest {

    @Test
    void shouldNotBuildWithNegativePage() {
        assertThatThrownBy(() -> new VddPageable(-1, 10))
            .isExactlyInstanceOf(InvalidNumberValueException.class)
            .hasMessageContaining("page");
    }

    @Test
    void shouldNotBuildWithPageSizeAtZero() {
        assertThatThrownBy(() -> new VddPageable(0, 0))
            .isExactlyInstanceOf(InvalidNumberValueException.class)
            .hasMessageContaining("pageSize");
    }

    @Test
    void shouldNotBuildWithPageSizeOverHundred() {
        assertThatThrownBy(() -> new VddPageable(0, 101))
            .isExactlyInstanceOf(InvalidNumberValueException.class)
            .hasMessageContaining("pageSize");
    }

    @Test
    void shouldGetFirstPageInformation() {
        VddPageable pageable = new VddPageable(0, 15);

        assertThat(pageable.page()).isZero();
        assertThat(pageable.pageSize()).isEqualTo(15);
        assertThat(pageable.offset()).isZero();
    }

    @Test
    void shouldGetPageableInformation() {
        VddPageable pageable = new VddPageable(2, 15);

        assertThat(pageable.page()).isEqualTo(2);
        assertThat(pageable.pageSize()).isEqualTo(15);
        assertThat(pageable.offset()).isEqualTo(30);
    }
}
