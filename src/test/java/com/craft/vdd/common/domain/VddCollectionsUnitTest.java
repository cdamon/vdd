package com.craft.vdd.common.domain;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Test;

class VddCollectionsUnitTest {

    @Test
    void shouldGetEmptyMapFromNullMap() {
        assertThat(VddCollections.immutable((Map<?, ?>) null)).isEmpty();
    }

    @Test
    void shouldGetUnmodifiableMap() {
        Map<Object, Object> value = new HashMap<>();
        value.put("key", "value");

        Map<Object, Object> result = VddCollections.immutable(value);

        assertThat(result).containsExactly(entry("key", "value"));
        assertThatThrownBy(() -> result.clear()).isExactlyInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void shouldGetEmptyMutableListFromNullList() {
        List<Object> list = VddCollections.mutable(null);

        assertThat(list).isEmpty();
        assertThatCode(() -> list.clear()).doesNotThrowAnyException();
    }

    @Test
    void shouldGetMutableListFromImutableList() {
        List<Object> list = VddCollections.mutable(List.of("pouet"));

        assertThat(list).containsExactly("pouet");
        assertThatCode(() -> list.clear()).doesNotThrowAnyException();
    }

    @Test
    void shouldGetEmptyMutableSetFromNullSet() {
        Set<String> set = VddCollections.mutable((Set<String>) null);

        assertThat(set).isEmpty();
        assertThatCode(() -> set.clear()).doesNotThrowAnyException();
    }

    @Test
    void shouldGetMutableSetFromImutableSet() {
        Set<Object> set = VddCollections.mutable(Set.of("pouet"));

        assertThat(set).containsExactly("pouet");
        assertThatCode(() -> set.clear()).doesNotThrowAnyException();
    }

    @Test
    void shouldGetEmptyImmutableListFromNullList() {
        List<String> element = null;
        List<String> list = VddCollections.immutable(element);

        assertThat(list).isEmpty();
        assertThatThrownBy(() -> list.clear()).isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void shouldGetImmutableListFromMutableList() {
        ArrayList<Object> mutablelist = new ArrayList<>();
        mutablelist.add("toto");
        List<Object> list = VddCollections.immutable(mutablelist);

        assertThat(list).containsExactly("toto");
        assertThatThrownBy(() -> list.clear()).isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void shouldGetEmptyImmutableSetFromNullSet() {
        Set<String> element = null;
        Set<String> set = VddCollections.immutable(element);

        assertThat(set).isEmpty();
        assertThatThrownBy(() -> set.clear()).isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void shouldGetImmutableSetFromMutableSet() {
        Set<Object> mutableSet = new HashSet<>();
        mutableSet.add("toto");
        Set<Object> list = VddCollections.immutable(mutableSet);

        assertThat(list).containsExactly("toto");
        assertThatThrownBy(() -> list.clear()).isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void shouldGetEmptyImmutableCollectionFromNullCollection() {
        Collection<String> element = null;
        Collection<String> result = VddCollections.immutable(element);

        assertThat(result).isEmpty();
        assertThatThrownBy(() -> result.clear()).isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void shouldGetImmutableCollectionFromMutableCollection() {
        Collection<Object> mutablelist = new ArrayList<>();
        mutablelist.add("toto");
        Collection<Object> result = VddCollections.immutable(mutablelist);

        assertThat(result).containsExactly("toto");
        assertThatThrownBy(() -> result.clear()).isInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void shouldGetEmptyStreamFromNullValue() {
        assertThat(VddCollections.streamOf(null)).isEmpty();
    }

    @Test
    void shouldGetStreamFromActualValue() {
        assertThat(VddCollections.streamOf("pouet")).containsExactly("pouet");
    }

    @Test
    void shouldGetEmptyStreamFromNullCollection() {
        assertThat(VddCollections.stream(null)).isEmpty();
    }

    @Test
    void shouldGetStreamFromCollection() {
        assertThat(VddCollections.stream(List.of("pouet")).toList()).containsExactly("pouet");
    }
}
