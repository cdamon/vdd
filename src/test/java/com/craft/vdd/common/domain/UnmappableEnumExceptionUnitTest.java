package com.craft.vdd.common.domain;

import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.error.domain.ErrorStatus;
import com.craft.vdd.error.domain.StandardErrorKey;
import com.craft.vdd.kipe.Resource;
import org.junit.jupiter.api.Test;

class UnmappableEnumExceptionUnitTest {

    @Test
    void shouldGetExceptionInformation() {
        UnmappableEnumException exception = new UnmappableEnumException(Resource.class, StandardErrorKey.class);

        assertThat(exception.key()).isEqualTo(StandardErrorKey.TECHNICAL_ERROR);
        assertThat(exception.message()).contains("Resource").contains("StandardErrorKey");
        assertThat(exception.status()).isEqualTo(ErrorStatus.INTERNAL_SERVER_ERROR);
    }
}
