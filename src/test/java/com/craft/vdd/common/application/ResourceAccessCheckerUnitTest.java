package com.craft.vdd.common.application;

import static com.craft.vdd.TestAuthentications.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.authentication.domain.Authorities;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class ResourceAccessCheckerUnitTest {

    private static final ResourceAccessChecker checker = new ResourceAccessChecker();

    @ParameterizedTest
    @ValueSource(strings = { Authorities.ADMIN })
    void shouldAuthorizeManagersReadForRole(String role) {
        assertThat(checker.can(user(role), "read", "MANAGERS")).isTrue();
    }

    @Test
    void shouldNotAuthorizeManagersReadForUser() {
        assertThat(checker.can(user(), "read", "MANAGERS")).isFalse();
    }
}
