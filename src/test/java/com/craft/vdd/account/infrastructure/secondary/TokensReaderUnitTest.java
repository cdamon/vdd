package com.craft.vdd.account.infrastructure.secondary;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.craft.vdd.account.domain.Account;
import com.craft.vdd.account.domain.Authority;
import com.craft.vdd.account.domain.Email;
import com.craft.vdd.account.domain.Firstname;
import com.craft.vdd.account.domain.Lastname;
import com.craft.vdd.authentication.domain.Username;
import com.craft.vdd.authentication.infrastructure.primary.NotAuthenticatedUserException;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

class TokensReaderUnitTest {

    private static final TokensReader tokens = new TokensReader();

    private final SecurityContext context = SecurityContextHolder.getContext();

    @Test
    void shouldNotReadUserInformationWithoutAuthenticatedUser() {
        context.setAuthentication(null);

        assertThatThrownBy(() -> tokens.read()).isExactlyInstanceOf(NotAuthenticatedUserException.class);
    }

    @Test
    void shouldNotGetInformationFromStringPrincipal() {
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn("hey");

        context.setAuthentication(authentication);

        assertThatThrownBy(() -> tokens.read()).isExactlyInstanceOf(NotAuthenticatedUserException.class);
    }

    @Test
    void shouldGetEmptyAuthoritiesFromJWTTokenWithoutAuthorities() {
        JwtAuthenticationToken token = new JwtAuthenticationToken(mock(Jwt.class));
        when(token.getTokenAttributes())
            .thenReturn(Map.of("preferred_username", "user", "email", "mail@company.fr", "given_name", "JEAN", "family_name", "VALJEAN"));

        context.setAuthentication(token);

        Account user = tokens.read();

        assertThat(user.authorities()).isEmpty();
    }

    @Test
    void shouldReadUserFromJWTToken() {
        JwtAuthenticationToken token = new JwtAuthenticationToken(mock(Jwt.class), List.of(new SimpleGrantedAuthority("ROLE_PDS")));
        when(token.getTokenAttributes())
            .thenReturn(Map.of("preferred_username", "user", "email", "mail@company.fr", "given_name", "JEAN", "family_name", "VALJEAN"));

        context.setAuthentication(token);

        Account user = tokens.read();

        assertAccountInformation(user);
    }

    @Test
    void shouldGetEmptyAuthoritiesFromOAuth2TokenWithoutAuthorities() {
        OAuth2User principal = mock(OAuth2User.class);
        when(principal.getName()).thenReturn("user");
        OAuth2AuthenticationToken token = new OAuth2AuthenticationToken(principal, null, "id");
        when(principal.getAttributes()).thenReturn(Map.of("email", "mail@company.fr", "given_name", "JEAN", "family_name", "VALJEAN"));

        context.setAuthentication(token);

        Account user = tokens.read();

        assertThat(user.authorities()).isEmpty();
    }

    @Test
    void shouldReadUserFromOAuth2Token() {
        OAuth2User principal = mock(OAuth2User.class);
        when(principal.getName()).thenReturn("user");
        OAuth2AuthenticationToken token = new OAuth2AuthenticationToken(principal, List.of(new SimpleGrantedAuthority("ROLE_PDS")), "id");
        when(principal.getAttributes()).thenReturn(Map.of("email", "mail@company.fr", "given_name", "JEAN", "family_name", "VALJEAN"));

        context.setAuthentication(token);

        Account user = tokens.read();

        assertAccountInformation(user);
    }

    private void assertAccountInformation(Account user) {
        assertThat(user.username()).isEqualTo(new Username("user"));
        assertThat(user.email()).isEqualTo(new Email("mail@company.fr"));
        assertThat(user.firstname()).isEqualTo(new Firstname("JEAN"));
        assertThat(user.lastname()).isEqualTo(new Lastname("VALJEAN"));
        assertThat(user.authorities()).containsExactly(new Authority("ROLE_PDS"));
    }
}
