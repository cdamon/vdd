package com.craft.vdd.account.domain;

import static com.craft.vdd.account.domain.AccountsFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.craft.vdd.authentication.domain.Username;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;

class AccountUnitTest {

    @Test
    void shouldGetEmptyAuthoritiesWithoutAuthorities() {
        assertThat(adminBuilder().authorities(null).build().authorities()).isEmpty();
    }

    @Test
    void shouldGetUnmodifiableAuthorities() {
        assertThatThrownBy(() -> adminBuilder().authorities(new ArrayList<>()).build().authorities().clear())
            .isExactlyInstanceOf(UnsupportedOperationException.class);
    }

    @Test
    void shouldGetAccountInformation() {
        Account account = admin();

        assertThat(account.username()).isEqualTo(new Username("admin"));
        assertThat(account.email()).isEqualTo(new Email("admin@vdd.com"));
        assertThat(account.firstname()).isEqualTo(new Firstname("Pierre"));
        assertThat(account.lastname()).isEqualTo(new Lastname("IAmAdmin"));
        assertThat(account.authorities()).containsExactly(new Authority("ROLE_ADMIN"));
        assertThat(account.enabled()).isTrue();
    }
}
