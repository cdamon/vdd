package com.craft.vdd.account.domain;

public final class PersonIdentitiesFixture {

    private PersonIdentitiesFixture() {}

    public static Name name() {
        return new Name(firstname(), lastname());
    }

    public static final Firstname firstname() {
        return new Firstname("Paul");
    }

    public static final Lastname lastname() {
        return new Lastname("DUPOND");
    }
}
