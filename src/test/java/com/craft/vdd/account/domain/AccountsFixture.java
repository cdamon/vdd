package com.craft.vdd.account.domain;

import com.craft.vdd.account.domain.Account.AccountBuilder;
import java.util.List;

public final class AccountsFixture {

    private AccountsFixture() {}

    public static Account admin() {
        return adminBuilder().build();
    }

    public static AccountBuilder adminBuilder() {
        return Account
            .builder()
            .username("admin")
            .email("admin@vdd.com")
            .firstname("Pierre")
            .lastname("IAmAdmin")
            .authorities(List.of("ROLE_ADMIN"))
            .enabled(true);
    }
}
