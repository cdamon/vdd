Feature: Users accounts management

  Scenario: Should not get account information for not authenticated user
    Given I am not logged in
    When I get my account information
    Then I should not be authorized

  Scenario: Should get account information
    Given I am logged in as "admin"
    When I get my account information
    Then I should have authorities "ROLE_ADMIN"