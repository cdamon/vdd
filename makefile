.PHONY: dev

dev: start-local-stack start-webservices
stop: stop-local-stack stop-webservices

install:
	npm i
clean: clean-webservices

start-webservices: stop-webservices
	cd . && { ./mvnw & echo $$! > webservices.pid;} > webservices.log &
	echo "Webservices starting (can take a few minutes, cf tail -f webservices.log) \n"
stop-webservices: 
ifneq ("$(wildcard webservices.pid)","")
	-kill `cat webservices.pid` && rm webservices.pid
	echo "Webservices stopping \n"
endif
clean-webservices:
	./mvnw clean

start-local-stack:
	docker-compose -f src/main/docker/local.yml up -d > stack.log
	echo "Local stack starting \n"
stop-local-stack:
	docker-compose -f src/main/docker/local.yml down --remove-orphans
	echo "Local stack stopping \n"

watch-logs:
	tail -f stack.log webservices.log

